---
source: TrendForum
created: '2014-10-25'
author: Jürgen
lang: nl
title: De kunst van het vreedzaam vechten
categories:
- Globalisering
- Maatschappij
---
‘Nooit eerder in de geschiedenis zaten de levens van mensen zo vol met conflicten. We moeten voortdurend ons mannetje staan, of ons vrouwtje – op straat, op het werk, in winkels en zelfs thuis. Het gekke is: ondanks al die confrontaties vloeit er maar zelden bloed. Nooit eerder zelfs was een beschaving zo effectief in het bewaren van vrede. Hoe lukt dat in onze moderne samenleving, wat is het geheim? Daarnaar gaan wij in dit boek op zoek.’ – Hans Achterhuis en Nico Koning

Een wereld zonder conflicten is niet alleen ondenkbaar, maar ook onwenselijk. Botsingen van tegengestelde meningen en belangen zijn een groot goed in een open samenleving. De vraag is alleen: hoe gaan we hiermee om en welke middelen zetten we in? Of anders gezegd: hoe gaan we krachtig het conflict aan en zorgen we er tegelijkertijd voor dat het niet uit de hand loopt? Dat geldt voor allerlei krachtmetingen in het openbare leven – denk aan sport, wetenschap, rechtspraak, politiek – en nog meer voor internationale crisissituaties zoals die in Oekraïne en Irak. ‘Vreedzaam vechten’ is een kunst die vrede en welvaart met zich meebrengt, zo leert de geschiedenis.

In dit [boek](https://lemniscaat.nl/Non-fictie/Filosofie/titels/9789047702191/De%20kunst%20van%20het%20vreedzaam%20vechten) gaan Hans Achterhuis en Nico Koning terug naar de vroege bronnen van geweldbeteugeling, uiteenlopend van het Gilgamesj-epos en de klassieke Oudheid tot de joods-christelijke traditie.
