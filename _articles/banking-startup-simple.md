---
source: TechSquare
created: '2014-01-22'
author: Jürgen
title: 'Banking startup: Simple'
categories:
- Cloud
- Devices
---
The [startup](https://www.techrepublic.com/article/appetite-for-disruption-can-simple-and-the-web-reinvent-banking/) [Simple](https://www.simple.com/) is an internet-only consumer bank with an [important difference](https://www.slate.com/blogs/business_insider/2014/01/19/simple_online_banking_review.html):
"Although Simple is designed to replace your bank (and I'd argue it does so quite well), it's not a proper bank itself. Your money sits in FDIC-insured accounts maintained by Simple's banking partners, but this is distinctly separate from what Simple really is—a company that provides free tools for checking your balance, depositing checks, tagging and categorizing your purchases, and even sending paper checks through the mail at no cost."
[W](https://en.wikipedia.org/wiki/Simple_%28bank%29)

Ripple:

- The open-source payment protocol: [Ripple](https://en.wikipedia.org/wiki/Ripple_(payment_protocol))
- [Ripple](https://ripple.com/protocol/) as promoted by Ripple Labs Inc.
