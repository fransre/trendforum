---
source: TrendForum
created: '2014-01-27'
author: Jürgen
lang: nl
title: Het nieuwe organiseren
categories:
- Management
---
Jaap Peters [kijkt terug](https://centuryoftalent.wordpress.com/2014/01/19/inspiratievideo-jaap-peters-over-transitie-en-organisatieontwikkeling/) naar [Frederick Winslow Taylor](https://en.wikipedia.org/wiki/Frederick_Winslow_Taylor): systemen boven mensen = scientific management!
