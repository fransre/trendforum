---
source: TechSquare
created: '2015-05-05'
author: Jürgen
title: Challenges in embedded systems architecture & architecting
categories:
- Architecture
---
[Gerrit Muller](https://redesign.esi.nl/about-esi/organisation/research-fellows/gerrit-muller.dot) gives an [introduction](https://www.youtube.com/watch?v=FEejRA2hmXs) to embedded systems architecture and embedded systems architecting, and the challenges for both of them.
He uses the [CAFCR model](https://www.gaudisite.nl/info/BasicCAFCR.info.html) as the main means to organize the information of the architecture.
