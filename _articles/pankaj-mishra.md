---
source: TrendForum
created: '2015-12-12'
author: Jürgen
title: Pankaj Mishra
categories:
- Globalisering
- Maatschappij
---
[Pankaj Mishra](https://en.wikipedia.org/wiki/Pankaj_Mishra) is an Indian author and writer of literary and political essays. His books include [Temptations of the West: How to Be Modern in India, Pakistan and Beyond](https://www.amazon.com/Temptations-West-Modern-Pakistan-Beyond/dp/0312426410) and [An End to Suffering: The Buddha in the World](https://www.amazon.com/End-Suffering-Buddha-World/dp/0312425090). His latest book is [From the Ruins of Empire: The Revolt Against the West and the Remaking of Asia](https://www.amazon.com/Ruins-Empire-Revolt-Against-Remaking/dp/1250037719).
[His articles](https://www.theguardian.com/profile/pankajmishra) with The Guardian.

[Discussion](https://www.lrb.co.uk/v33/n21/pankaj-mishra/watch-this-man) with Niall Ferguson about his character or that of his books?
