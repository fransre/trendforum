---
source: TechSquare
created: '2013-08-17'
author: Jürgen
title: Nothing to hide
categories:
- Privacy
---
Daniel J. Solove [explains](https://chronicle.com/article/Why-Privacy-Matters-Even-if/127461/) Why Privacy Matters Even if You Have 'Nothing to Hide'

SEP about [privacy](https://plato.stanford.edu/entries/privacy/)
