---
source: TrendForum
created: '2009-01-03'
author: Jürgen
title: Culture of Improvement
categories:
- Geschiedenis
- Globalisering
- Wetenschap
- Techniek
---
![Robert Friedel](https://www.history.umd.edu/Images/photo_friedel.JPG)

Nice review of [Robert Friedels](https://www.history.umd.edu/Bio/friedel.html) book: [A Culture of Improvement: Technology and the Western Millennium](https://www.amazon.com/Culture-Improvement-Technology-Western-Millennium/dp/0262062623/)
by [The NYT](https://www.nytimes.com/2007/05/21/arts/21conn.html?_r=2&oref=slogin&pagewanted=print&oref=slogin)
