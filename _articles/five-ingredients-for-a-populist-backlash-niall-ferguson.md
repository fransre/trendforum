---
source: TrendForum
created: '2016-11-19'
author: Jürgen
title: Five ingredients for a populist backlash - Niall Ferguson
categories:
- Globalisering
- Maatschappij
---
[Five ingredients for a populist backlash](https://www.youtube.com/watch?v=pmdxYTyrI-E) - Niall Ferguson - Zeitgeist 2016

- rising immigration
- increase in inequality
- perception of corruption
- major financial crisis
- enter the demagogue ([Denis Kearney](https://en.wikipedia.org/wiki/Denis_Kearney), [1882](https://en.wikipedia.org/wiki/Chinese_Exclusion_Act))

Niall Ferguson: [Donald Trump Not A Fascist But A Populist](https://www.youtube.com/watch?v=Ybae3LF36_k)
