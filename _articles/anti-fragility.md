---
source: TrendForum
created: '2016-06-05'
author: Jürgen
title: Anti-fragility
categories:
- Techniek
- Financiën
---
Antifragile: Things That Gain From Disorder - Nassim Taleb - [Animated Book Review](https://www.youtube.com/watch?v=HKCtSzpYrs4)

* In Antifragile: Things That Gain From Disorder, Nassim Taleb explains the concept of antifragility. Everything that is alive, and everything that stays alive displays some sort of antifragility. The stressors lead the individual to success, however there are rules to the game. The stressors can not be too much to overload the system. Also, the antifragile patient must be able to comfortably recover from the stressors. If you apply this concept to your life, you will gain great success in a matter of a few short months. Simply start by looking at every situation a thing you can gain from. Nassim Taleb did a great job writing this book, covering a multitude of topics and even including some humor.

[Antifragile](https://www.amazon.com/Antifragile-Things-That-Disorder-Incerto/dp/0812979680): Things That Gain from Disorder by [Nassim Nicholas Taleb](https://en.wikipedia.org/wiki/Nassim_Nicholas_Taleb)

* His [homepage](https://www.fooledbyrandomness.com/)
* Real World Risk [Institute](https://realworldrisk.com)
