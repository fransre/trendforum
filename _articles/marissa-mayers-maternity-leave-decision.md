---
source: TrendForum
created: '2015-09-03'
author: Jürgen
title: Marissa Mayer's Maternity Leave Decision
categories:
- Globalisering
- Maatschappij
- Management
---
The Yahoo CEO announced yesterday that she plans to take [just 14 days of maternity leave](https://www.linkedin.com/pulse/how-marissa-mayers-maternity-decision-affects-young-women-fairchild) in December after the birth of her identical twin girls.
