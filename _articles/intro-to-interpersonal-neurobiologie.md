---
source: TrendForum
created: '2011-02-12'
author: Jürgen
title: Intro to Interpersonal Neurobiologie
categories:
- Wetenschap
---
David Siegel at a [talk at TED](https://www.youtube.com/watch?v=Nu7wEr8AnHw). It is a good start before seeing more of YT videos about him.

Another longer [video](https://fora.tv/2009/06/30/Dan_Siegel_The_Brain_and_the_Developing_Mind#fullprogram) with large overlap.
