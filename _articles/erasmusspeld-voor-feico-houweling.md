---
source: TrendForum
created: '2015-11-26'
author: Jürgen
lang: nl
title: Erasmusspeld voor Feico Houweling
categories:
- Geschiedenis
- Maatschappij
---
Erasmusspeld [voor](https://www.persberichtenrotterdam.nl/bericht/1166/Erasmusspeld-voor-Feico-Houweling/) Feico Houweling
en zijn [dankwoord](https://www.feico-houweling.nl/70-dankwoord-erasmusspeld).
