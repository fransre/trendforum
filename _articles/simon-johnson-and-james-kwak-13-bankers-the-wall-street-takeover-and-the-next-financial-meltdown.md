---
source: TrendForum
created: '2010-12-21'
author: Jürgen
title: 'Simon Johnson and James Kwak: 13 Bankers: The Wall Street Takeover and the
  Next Financial Meltdown'
categories:
- Globalisering
---
Johnson and Kwak are looking at the 6 major American banks (Citi Group, Goldman Sachs, Morgan Stanley, J.P. Morgan Chase, Bank of America, Wells Fargo) as they are building an oligarchy (political power based on financial power). They control 60% of the US GNP.

[BILL MOYERS JOURNAL | James Kwak and Simon Johnson Pt 1 | PBS](https://www.youtube.com/watch?v=itFl9MEHXzo)

[BILL MOYERS JOURNAL | James Kwak and Simon Johnson Pt 2 | PBS](https://www.youtube.com/watch?v=NjmTiz3qWh0)

The current incentive structure lets them take irresponsible risks because the government has to bail them out.
Any bank above $100 billion is too big too fail and should be broken up and on the short term be heavily taxed.

[Interesting list of supporters](https://13bankers.com/) of [their book](https://www.amazon.com/13-Bankers-Takeover-Financial-Meltdown/dp/030747660X).
[Their website](https://baselinescenario.com/) includes a section: Financial crisis for beginners.

I love this line: "The American democracy was not given to us on a platter, it is not ours for all time irrespective of our efforts. Either people organise and they find political leadership to take this on, or we are going to be in big trouble, ok." (Pt 1, 25:15 min.).

"Unnecessary complexity just creates rich opportunities for systemic corruption." (Pt 2, 11:10 min.).

[Bureaucratic Complexity and Impacts of Corruption in Utilities](https://hdl.handle.net/10202/339)

[Corruption and trade tariffs, or a case for uniform tariffs](https://ideas.repec.org/p/wbk/wbrwps/2216.html)

this size limit may be valid for all infrastructure companies, like [broadband providers](https://www.itnews.com.au/News/249441,optus-calls-to-break-nbn-co-into-baby-telcos.aspx) (though it talks only about competition).

![Doonesbury Comic for 04/03/2011](https://www.arcamax.com/newspics/17/1774/177413.jpg)
