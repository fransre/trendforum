---
source: TrendForum
created: '2015-11-21'
author: Jürgen
title: OnePlanetArchitecture institute
categories:
- Globalisering
- Techniek
- Maatschappij
---
The oneplanetarchitecture institute [OPAi](https://www.opai.eu/) was founded by Thomas Rau on 24.12.2008. It is a task-oriented network for the design of sustainable concepts, products and organisations.

[workbook](https://www.opai.eu/uploads/Guided_Choices_towards_a_Circular_Business_Model_pdf1.pdf) ‘Circular Economy’ for small and medium enterprises
