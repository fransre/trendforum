---
source: TrendForum
created: '2010-11-16'
author: Toon
title: Base of Pyramid Innovation Center
categories:
- Ontwikkelingssamenwerking
---
A new partnership that is promoting market based solutions and entrepreneurship in developing countries. The launch of this partnership is introduced by [Africa Interactive](https://www.africa-interactive.com).

[Video](https://vimeo.com/15589771)
