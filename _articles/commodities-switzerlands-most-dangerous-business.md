---
source: TrendForum
created: '2011-09-25'
author: Jürgen
title: 'Commodities: Switzerland’s Most Dangerous Business'
categories:
- Globalisering
---
Unnoticed by the public and politicians, Switzerland has become the world’s most important commodities hub. Trade in oil, gas, coal, metals and agricultural products – particularly via deals made in Geneva and Zug – has grown by an incredible 1,500 percent since 1998, according to [Berne Declaration's investigations](https://www.evb.ch/en/p25019512.html). The result: Seven of the twelve corporations with the highest turnover in Switzerland trade in, and/or mine, commodities. Switzerland has become a global commodity hub thanks to its mix of tax privileges, a strong financial sector, weak regulation and lax embargo policy.
