---
source: TrendForum
created: '2015-09-20'
author: Jürgen
title: The Brussels Effect
categories:
- Globalisering
- Maatschappij
- Financiën
---
It is common to hear Europe described today as the power of the past. Europe is perceived to be weak militarily. Its relative economic power is declining as Asia’s is rising. Its common currency may be on the verge of disintegrating. On the world stage, the European Union is thought to be waning into irrelevance due to its inability to speak with one voice.

Contrary to this prevalent perception, [this article](https://scholarlycommons.law.northwestern.edu/cgi/viewcontent.cgi?article=1081&context=nulr) highlights a deeply underestimated aspect of European power that the discussion on globalization and power politics overlooks: Europe’s unilateral power to regulate global markets. The European Union sets the global rules across a range of areas, such as food, chemicals, competition, and the protection of privacy. EU regulations have a tangible impact on the everyday lives of citizens around the world. Few Americans are aware that EU regulations determine the makeup they apply in the morning, the cereal they eat for breakfast, the software they use on their computer, and the privacy settings they adjust on their Facebook page. And that’s just before 8:30 AM. The EU also sets the rules governing the interoffice phone directory they use to call a coworker. EU regulations dictate what kind of air conditioners Americans use to cool their homes and why their children no longer find soft plastic toys in their McDonald’s Happy Meals. This phenomenon is called the “Brussels Effect”.

Contrary to the Delaware effect (lower regulatory standards determine the market: race to the bottom hypothesis) trade often follows the California effect (higher regulatory standards determine the market). The Brussels effect is the California effect in a global context, what California is doing to the other states of the US, Brussels is doing to the rest of the world.

When does this happen? Which kind of laws are subject to the Brussels effect?

- anti-trust law
- privacy law
- environmental law
- health
- food safety

What is an underlying theory why this unilateral regulation can take place, and why the EU is the source of those laws:

- large market
- regulatory capacity
- preference for strict rules
- regulate inelastic targets (e.g. consumer markets iso capital markets)
- non-divisible standards (e.g. legal non-divisiblity, technical non-divisibility leads to economic non-divisibility because of economy of scale)

These are the conditions of the Brussels effect:
[see](https://www.youtube.com/watch?v=QeFmR3Fkjzw) Anu Bradford: The Brussels Effect: The Rise of a Regulatory Superstate in Europe

