---
source: Enkairoi
created: '2009-01-03'
author: Jürgen
title: Child
categories:
---

### Rights of Children

[Convention on the Rights of the Child](https://www.unicef.org/child-rights-convention) [Wikipedia](https://en.wikipedia.org/wiki/Convention_on_the_Rights_of_the_Child)

### Continuous Development

[Charter on the Rights of the Child Before, During and After Birth](https://www.ohchr.org/en/professionalinterest/pages/crc.aspx) talks about a continuous development of a child before, during and after birth. Many conditions later in life can be traced back to events before birth. Examples are ..
An [article](https://www.spiegel.de/wissenschaft/mensch/schwangerschaft-unerwuenschte-kinder-haben-mehr-angst-a-594587.html) in German describes new research results. They are also based on data from the Dutch "hunger winter" 1944/45.

[Ludwig Janus](http://www.ludwig-janus.de/) is one of the leading researchers. Read also the Amazon review of this book [The Enduring Effects of Prenatal Experience: Echoes from the Womb](https://www.amazon.com/Enduring-Effects-Prenatal-Experience-Echoes/dp/1568218532). Another [review](http://primal-page.com/janus.htm).\
[Levine](/levine/) describes and recommends re-negotiation of traumatic experiences instead of re-enactment of them. He considers reenactment to be dangerous.
