---
source: TechSquare
created: '2022-02-27'
author: Jürgen
title: Starlink
categories:
- Architecture
- Devices
---
[Starlink](https://en.wikipedia.org/wiki/Starlink) is a satellite internet constellation operated by [SpaceX](https://en.wikipedia.org/wiki/SpaceX) providing satellite Internet access coverage to most of the Earth. The constellation has grown to over 1,700 satellites through 2021, and will eventually consist of many thousands of mass-produced small satellites in [low Earth orbit](https://en.wikipedia.org/wiki/Low_Earth_orbit) (LEO), which communicate with designated ground transceivers.

Between February 2018 and 2022, SpaceX successfully launched 2,091 satellites into orbit. In March 2020, SpaceX reported producing six satellites per day.

2022-02-27 news:
- On 26 February 2022 Elon Musk announced that SpaceX’s Starlink satellites had become active over Ukraine after a request from the Ukrainian government to replace internet services destroyed during the 2022 Russian invasion of Ukraine.
