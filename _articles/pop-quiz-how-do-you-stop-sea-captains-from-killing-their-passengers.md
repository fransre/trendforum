---
source: TrendForum
created: '2010-10-29'
author: Frans
title: 'Pop Quiz: How Do You Stop Sea Captains From Killing Their Passengers?'
categories:
- Geschiedenis
---
![Prisonship](https://media.npr.org/assets/img/2010/09/09/prisonship.jpg?t=1284068935&s=3)

Back in the 1700s, the British government paid sea captains to take felons to Australia. At first, it didn't work so well: About a third of the males on one particularly horrific voyage died. The rest arrived beaten, starved, and sick.

[Pop Quiz: How Do You Stop Sea Captains From Killing Their Passengers?](https://www.npr.org/blogs/money/2010/09/09/129757852/pop-quiz-how-do-you-stop-sea-captains-from-killing-their-passengers)

Lesson: A good social order aligns self-interest with social interest.

Or: Economics should serve ethics.

great example of incentives matter!
