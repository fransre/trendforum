---
source: TrendForum
created: '2013-04-13'
author: Jürgen
title: How pretty a woman has to be
categories:
- Maatschappij
---
Katie Makkai answers a [question](https://www.upworthy.com/this-womans-beef-with-prettiness-will-leave-you-speechless) she's had since she was very young in exactly the kind of way that slams into your gut and stays there.
