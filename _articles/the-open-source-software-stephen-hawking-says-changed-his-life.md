---
source: TechSquare
created: '2014-12-04'
author: Jürgen
title: The open-source software Stephen Hawking says changed his life
categories:
- Human Technical-System Interfacing
---
The famous physicist talks about [how he spent three years](https://www.techrepublic.com/article/how-a-software-upgrade-is-giving-professor-stephen-hawking-a-new-lease-of-life/) working with Intel to devise new software that makes it simpler for him to communicate with the world.
