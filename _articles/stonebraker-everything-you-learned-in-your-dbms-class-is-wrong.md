---
source: TechSquare
created: '2014-04-17'
author: Jürgen
title: 'Stonebraker: Everything You Learned in Your DBMS Class is Wrong'
categories:
- Baseline
- Scalability
---
Stonebraker: One Size Fits None - (Everything You Learned in Your DBMS Class is Wrong)
Great [presentation](https://slideshot.epfl.ch/play/suri_stonebraker) about the design (logic) of the NewSQL-DB VoltDB and why all traditional DBMS wisdom is wrong.

Referenced Concepts:

- [Spanner](https://en.wikipedia.org/wiki/Spanner_(distributed_database_technology))
- [ARIES](https://en.wikipedia.org/wiki/Algorithms_for_Recovery_and_Isolation_Exploiting_Semantics)
- [VoltDB](https://en.wikipedia.org/wiki/VoltDB)
- [Impala](https://en.wikipedia.org/wiki/Cloudera_Impala)
- [NewSQL](https://en.wikipedia.org/wiki/NewSQL)

[Anti-Caching](https://techtv.mit.edu/collections/nedb2013/videos/22715-nedb-2013-session-1-justin-debrabant) by DeBrabant

Column-based databases:
International innovation award [to](https://www.cwi.nl/news/2014/international-innovation-award-big-data-research-martin-kersten) Martin Kersten
