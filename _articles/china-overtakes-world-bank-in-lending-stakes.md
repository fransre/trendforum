---
source: TrendForum
created: '2011-01-18'
author: Jürgen
title: China overtakes World Bank in lending stakes
categories:
- Ontwikkelingssamenwerking
- Globalisering
---
As China becomes the [biggest lender](https://www.ft.com/cms/s/0/488c60f4-2281-11e0-b6a2-00144feab49a.html) how will that influence the political agendas of our development aid?
