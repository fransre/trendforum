---
source: TrendForum
created: '2014-04-22'
author: Jürgen
title: '"People in the past were very stressed out"'
categories:
- Geschiedenis
- Maatschappij
---
[People in the past](https://news.discovery.com/history/archaeology/ancient-peru-stress-hair-cortisol.htm) were very stressed out, suggests a new study that found high amounts of a stress hormone in the hair of Peruvian individuals who lived between 550 A.D. and 1532.
