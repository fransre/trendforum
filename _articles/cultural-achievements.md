---
source: Enkairoi
created: '2011-01-31'
author: Jürgen
title: Cultural Achievements
categories:
---

Throughout history mankind has a gained cultural achievements which characterize modern life. These achievements have precise origins in history.

We name some of them here.

***

The ladder of potentialities for progress and emancipation is shown in the following list:

| **CENTURY** |	**LIBERTIES** |	**PROTECTING PRINCIPLE** |	**CORRESPONDING INSTITUTION**
| 20th |	Freedom for growth, health |	Public character of labour |	(?Perhaps: adult education, decentralization of industry?)
| 19th |	Freedom for talent, thought, genius, speech, creativeness to compete |	Public character of private ideas |	Copyright, patents, a written constitution
| 17th |	Freedom of endowment |	Public character of wills |	An independent Judiciary
| 16th |	Free choice of profession, no vows for children |	Public character of education |	Public schools
| 13th |	Freedom of competition between teachers |	Public character of the sciences |	Universities
| 11th |	Freedom of movement for the men in the professions |	Public character of civil life (truce of God) |	Judges of the peace, public prosecution of crime

taken from: Eugen Rosenstock-Huessy: Out of Reveolution, p. 32

***
Kroesen, Ravesteijn: Radical sustainable change: a case for communicative sustainability and civil society as enabling environment for engineering

Oualities engineers have:
- Future-orientedness: technology aims at a better future.

Other human qualities, likewise essential to engineering, include
- universality,
- civility,
- conscientiousness,
- teamspirit,
- rational inventiveness,
- large-scale planning and
- social responsibility.

***
Otto Kroesen: SUCCESSFUL CONTEXTUAL TECHNOLOGY TRANSFER AND DETERMINANTS OF CULTURE
and Een rode draad door de tijd

Tribal Culture Qualities:
- chief in the lead: external institution (chiefdom)
- internalized values (tradition and authority)
- family and incest taboe
- openness to other tribes for exchange of women (avoiding inbreeding)

Imperial Culture Qualities:
- hierarchy
- labor division

Jewish (OT) Qualities:
- humans are made in the image of God (first step toward individuals?)
- dethrone the gods or the cosmic order, representing traditional authority and hierarchy
- claim divine power beyond the visible powers, a divine power which is sensitive to the sufferings of ordinary human beings or which is not a divine power at all
- future oriented human achievements and qualities: prophetic calling
- religious relation is not determined by ancestors but for each generation anew (Abraham - Isaac)
- secure provisioning of food is not of ultimate value (Moses)
- established (political) structure can be criticized (David)
- humans are not captured by the tragedy of this cosmos, but can have personal relationship to God, who is beyond the cosmos (Job)

Ancient Greek Culture Qualities
- empathy: seeing the other as oneself beyond the social group
- reflective activities: science, theater, philosophy, sports

Christian / Western Culture Qualities:
- man is allowed to switch between the once exclusive cultural qualities of the tribe, the empire, Israel and ancient Greece. These qualities become aspects of a fuller set of qualities.
- social death is not the end of life, but man can be resurrected to a new life
- "engineers qualities above"
