---
source: TrendForum
created: '2020-11-01'
author: Toon
title: Netherlands Food Partnership
categories:
- Maatschappij
- Techniek
- Globalisering
- Food
---
The recently launched Netherlands Food Partnership enables powerful collaboration between relevant Dutch organisations and international partners to achieve urgent changes that contribute to sustainable food systems and nutrition security and reach SDG2 by 2030.

[About Netherlands Food Partnership](https://www.nlfoodpartnership.com/about/)

Food systems approach
---------------------
The global ambitions to end hunger, achieve food security and improved nutrition and promote sustainable agriculture (SDG2) demand a complex transition of the current food system. At the same time, the food system must also respond to challenges like climate change, resource scarcity, (youth) employment and gender equality. The multiple aspects of food production and consumption are closely interconnected and activities in one part of the food system can result in (unintended) outcomes in other areas of food, socio-economic or environmental systems. A food systems approach is expected to help manage such trade-offs and identify options for synergy.

[Food systems approach](https://www.nlfoodpartnership.com/knowledge-expertise/food-systems-approach/)
