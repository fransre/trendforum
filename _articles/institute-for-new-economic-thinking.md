---
source: TrendForum
created: '2013-01-15'
author: Jürgen
title: Institute for New Economic Thinking
categories:
- Globalisering
- Wetenschap
- Financiën
---
Another initiative from George Soros:
"The Institute for New Economic Thinking ([INET](https://ineteconomics.org/about)) was created to broaden and accelerate the development of new economic thinking that can lead to solutions for the great challenges of the 21st century.
The havoc wrought by our recent global financial crisis has vividly demonstrated the deficiencies in our outdated current economic theories, and shown the need for new economic thinking – right now."
[Introductory video](https://youtu.be/KoqLu5CKx-o)
