---
source: TechSquare
created: '2011-03-11'
author: Jürgen
title: 'Filter Bubble: Living in a personally tailored world'
categories:
- Architecture
- Human Technical-System Interfacing
---
The algorithmic filtering for personalizing information creates an Internet where algorithms decide for you what you see. Eli Pariser calls it [filter bubble](https://www.youtube.com/watch?v=LlWAXrS069Y).
