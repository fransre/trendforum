---
source: TrendForum
created: '2012-01-14'
author: Jürgen
title: What Would a European Tobin Tax Really Mean?
categories:
- Globalisering
- Financiën
---
Article about [Tobin Tax](https://www.spiegel.de/international/business/0,1518,druck-808592,00.html).
The Dutch CPB is [argueing against](https://www.nu.nl/economie/2698790/cpb-vindt-transactiebelasting-slecht-idee.html) such a tax.
