---
source: TrendForum
created: '2015-12-12'
author: Jürgen
title: In2Care
categories:
- Ontwikkelingssamenwerking
- Wetenschap
- Techniek
---
[In2Care](https://www.in2care.org/)'s mission is to create scientifically sound products against disease-transmitting insects to improve the well being of people. We focus on biological and affordable solutions to combat malaria, Dengue, West Nile Virus, Chikungunya and other diseases transmitted by mosquitoes.

In collaboration with partners in the US, UK and Africa, In2Care will participate in a $10.2-million study to test Eave tubes as a new malaria-prevention method. The study is funded by a [grant](https://www.nu.nl/gezondheid/4181596/nederlands-wapen-malaria-gesteund-met-miljoenen-bill-gates.html) from the Bill & Melinda Gates Foundation that was awarded to Penn State University in the USA. The team receives this five-year grant to investigate a new method for preventing the transmission of malaria. The method involves limiting mosquito access to houses by screening windows and installing “eave tubes” that contain a unique type of insecticide-laced netting developed by In2Care that kills the insects as they attempt to enter.
