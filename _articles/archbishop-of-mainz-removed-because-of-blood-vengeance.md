---
source: TrendForum
created: '2014-06-07'
author: Jürgen
title: Archbishop of Mainz removed because of blood vengeance
categories:
- Geschiedenis
- Kerk
- Maatschappij
---
In 745 [Gewiliobus/Gewilip](https://de.wikipedia.org/wiki/Gewiliobus) was removed as [Archbishop of Mainz](https://en.wikipedia.org/wiki/Archbishop_of_Mainz) because he killed with his own hands the murderer of his father with the words: "accipe quo patrem vindico ferrum" in line with Germanic warrior tradition. He was succeeded by [Saint Boniface](https://en.wikipedia.org/wiki/Saint_Boniface).

'We'll Get You': An Albanian Boy's Life [Ruined by](https://www.spiegel.de/international/world/blood-feuds-still-prevalent-in-albania-a-973498.html) Blood Feuds
