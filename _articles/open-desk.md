---
source: TechSquare
created: '2015-09-30'
author: Jürgen
title: Open Desk
categories:
- Devices
---
[Opendesk](https://www.opendesk.cc/) [W](https://en.wikipedia.org/wiki/OpenDesk) is an initiative to produce furniture on the principles of Open Making.
