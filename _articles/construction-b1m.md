---
source: TechSquare
created: '2019-05-19'
author: Jürgen
title: 'Construction: B1M'
categories:
- City
- Architecture
- Energy
- Infrastructure
---
The definitive [video channel](https://www.youtube.com/user/TheB1MLtd/videos) for construction! We love construction, and we want the whole world to love it too.
