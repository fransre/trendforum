---
source: TrendForum
created: '2010-11-23'
author: Jürgen
title: Christianity earlier in Asia than in Northern Europe
categories:
- Geschiedenis
- Kerk
- Globalisering
---
Christianity was earlier in Asia than in Northern Europe outside of the Roman Empire. This has implications for the role of West in world history. It seems that its transformations via revolutions are becoming more important in understanding how the West could reach its position.

For the historic background of the spread of Christianity some quotes from: [History of Eastern Christianity in Asia](https://en.wikipedia.org/wiki/History_of_Eastern_Christianity_in_Asia):

"Judging from the New Testament account of the rise and expansion of the early church, during the first few centuries of Christianity, the most extensive dissemination of the gospel was not in the West but in the East. In fact, conditions in the Parthian empire (250 BC - AD 226), which stretched from the Euphrates to the Indus rivers and the Caspian to the Arabian seas, were in some ways more favourable for the growth of the church than in the Roman world. And though opposition to Christianity increasingly mounted under successive Persian and Islamic rulers, Christian communities were eventually established in the vast territory which stretches from the Near to the Far East possibly as early as the first century of the church."

"For at least twelve hundred years the Church of the East was noted for its missionary zeal, its high degree of lay participation, its superior educational standards and cultural contributions in less developed countries, and its fortitude in the face of persecution."

"It is difficult to determine the exact time when the Christian gospel first reached China. The ancient Breviary of the Syrian church of Malabar (India) states that “By the means of St. Thomas the Chinese...were converted to the truth...By means of St. Thomas the kingdom of heaven flew and entered into China...The Chinese in commemoration of St. Thomas do offer their adoration unto Thy most Holy Name, O God.” Some authors have claimed to have found in a very ancient Taoist writing evidence of a spiritual awakening in China in the latter part of the 1st century."
