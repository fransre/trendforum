---
source: TrendForum
created: '2014-07-30'
author: Jürgen
title: The Second Machine Age
categories:
- Globalisering
- Techniek
- Maatschappij
---
[The Second Machine Age: Work, Progress, and Prosperity in a Time of Brilliant Technologies](https://www.amazon.com/Second-Machine-Age-Prosperity-Technologies/dp/0393239357)
by [Erik Brynjolfsson](https://en.wikipedia.org/wiki/Erik_Brynjolfsson) and [Andrew McAfee](https://en.wikipedia.org/wiki/Andrew_McAfee)

A revolution is under way.
In recent years, Google’s autonomous cars have logged thousands of miles on American highways and IBM’s Watson trounced the best human Jeopardy! players. Digital technologies—with hardware, software, and networks at their core—will in the near future diagnose diseases more accurately than doctors can, apply enormous data sets to transform retailing, and accomplish many tasks once considered uniquely human.

In The Second Machine Age MIT’s Erik Brynjolfsson and Andrew McAfee—two thinkers at the forefront of their field—reveal the forces driving the reinvention of our lives and our economy. As the full impact of digital technologies is felt, we will realize immense bounty in the form of dazzling personal technology, advanced infrastructure, and near-boundless access to the cultural items that enrich our lives.

Amid this bounty will also be wrenching change. Professions of all kinds—from lawyers to truck drivers—will be forever upended. Companies will be forced to transform or die. Recent economic indicators reflect this shift: fewer people are working, and wages are falling even as productivity and profits soar.

Drawing on years of research and up-to-the-minute trends, Brynjolfsson and McAfee identify the best strategies for survival and offer a new path to prosperity. These include revamping education so that it prepares people for the next economy instead of the last one, designing new collaborations that pair brute processing power with human ingenuity, and embracing policies that make sense in a radically transformed landscape.

A fundamentally optimistic book, The Second Machine Age will alter how we think about issues of technological, societal, and economic progress.
