---
source: TechSquare
created: '2011-10-27'
author: Jürgen
title: 'OpenCompute: Facebook''s open source HW'
categories:
- Cloud
---
The [OpenCompute](https://opencompute.org/) project site.

An [article](https://www.zdnet.com/blog/btl/facebook-open-sources-its-server-data-center-designs-hardware-fallout-to-follow/47045).

AOL new [micro datacenters](https://www.datacenterdynamics.com/focus/archive/2012/07/aol-brings-online-building-less-data-center) and the [story](https://loosebolts.wordpress.com/2012/07/05/aols-data-center-independence-day/) behind.
