---
source: TrendForum
created: '2014-09-21'
author: Jürgen
title: 'Intense World: A new theory about Autism'
categories:
- Wetenschap
- Maatschappij
---
The boy whose brain could unlock autism -
Autism changed Henry Markram’s family. Now his [Intense World theory](https://medium.com/matter-archive/the-boy-whose-brain-could-unlock-autism-70c3d64ff221) could transform our understanding of the condition.
