---
source: TrendForum
created: '2011-03-17'
author: Jürgen
title: The Five Stages of Renewal in the Local Church
categories:
- Kerk
---
Rick Warren speaks about [five (six) stages of renewal](https://www.christianpost.com/news/the-five-stages-of-renewal-in-the-local-church-32973/) of the pastor and his congregation. Stage 3 contains what the Lichtstad has called 5Vs.
