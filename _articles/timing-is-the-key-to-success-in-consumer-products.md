---
source: TechSquare
created: '2011-04-20'
author: Jürgen
title: Timing is the key to success in Consumer Products
categories:
- Devices
---
David Aaker looks at the [example of Apple](https://www.prophet.com/blog/aakeronbrands/28-apples-secret-weapon): one key ingredient that is usually overlooked is the ability of Jobs to get the timing right.
