---
source: TrendForum
created: '2013-09-29'
author: Willem
title: Human cognitive progress
categories:
- Overig
---
Moral philospher [James Flynn](https://www.otago.ac.nz/politicalstudies/flynn.html) explains in a [TED talk](https://www.otago.ac.nz/politicalstudies/flynn.html) how human cognitive abilities have grown in the past centuries, starting with his discovery that the average IQ has consistently increased since the first test.

What would be the "next thing" that we're learning now as society or world?

From a [review](https://www.amazon.com/Where-Good-Ideas-Come-From-ebook/product-reviews/B0046ZRZ30/ref=cm_cr_pr_top_helpful?ie=UTF8&showViewpoints=0&sortBy=byRankDescending) of [Where Good Ideas Come From: The Natural History of Innovation
](https://www.amazon.com/Where-Good-Ideas-Come-Innovation-ebook/dp/B0046ZRZ30)

....

As I read Steven Johnson's book, I realized why we struggle with how to be more creative.

......

Seven Key Principles

Mr. Johnson's engaging writing style guides us through seven key areas that must be understood in order to maximize our creativity, the key areas being:

1. The adjacent possible - the principle that at any given moment, extraordinary change is possible but that only certain changes can occur (this describes those who create ideas that are ahead of their time and whose ideas reach their ultimate potential years later).

2. Liquid networks - the nature of the connections that enable ideas to be born, to be nurtured and to blossom and how these networks are formed and grown.

3. The slow hunch - the acceptance that creativity doesn't guarantee an instant flash of insight but rather, germinates over time before manifesting.

4. Serendipity - the notion that while happy accidents help allow creativity to flourish, it is the nature of how our ideas are freely shared, how they connect with other ideas and how we perceive the connection at a specific moment that creates profound results.

5. Error - the realization that some of our greatest ideas didn't come as a result of a flash of insight that followed a number of brilliant successes but rather, that some of those successes come as a result of one or more spectacular failures that produced a brilliant result.

6. Exaptation - the principle of seizing existing components or ideas and repurposing them for a completely different use (for example, using a GPS unit to find your way to a reunion with a long-lost friend when GPS technology was originally created to help us accurately bomb another country into oblivion).

7. Platforms - adapting many layers of existing knowledge, components, delivery mechanisms and such that in themselves may not be unique but which can be recombined or leveraged into something new that is unique or novel.
.....
