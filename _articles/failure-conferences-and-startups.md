---
source: TechSquare
created: '2015-10-21'
author: Jürgen
title: Failure conferences and startups
categories:
- Cloud
- Scalability
- Devices
---
Why Coke is hiring founders [&](https://www.innovationleader.com/why-coke-is-hiring-entrepreneurs-and-organizing-failure-conferences/) holding failure conferences
