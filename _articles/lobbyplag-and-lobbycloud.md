---
source: TechSquare
created: '2014-05-07'
author: Jürgen
title: LobbyPlag and LobbyCloud
categories:
- Cloud
- Policy
---
Two websites which support political transparency:

[LobbyCloud](https://lobbycloud.eu/) is a library of influences on public policy. We publish lobby documents circulating in the EU bureaucracy.

[LobbyPlag](https://lobbyplag.eu/lp): Transparency for the EU
Currently the European Parliament is about to adopt a new General Data Protection Regulation (GDPR). Over 3000 amendments were submitted in the comittees involved with the regulation, hundreds of pages in proposals by lobby organisations had their impact on this highly volatile process. LobbyPlag aims to make this whole process transparent and comprehensible. You can compare the lobbyists requests and the committee members proposals and learn which impact the changes would have on the protection of your data.
