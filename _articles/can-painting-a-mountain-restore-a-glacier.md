---
source: TrendForum
created: '2011-05-27'
author: Jürgen
title: Can painting a mountain restore a glacier?
categories:
- Ontwikkelingssamenwerking
- Globalisering
- Maatschappij
---
Slowly but surely an extinct glacier in a remote corner of the Peruvian Andes is being returned to its former colour, not by falling snow or regenerated ice sheets, but by [whitewash](https://www.bbc.co.uk/news/10333304).
