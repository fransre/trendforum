---
source: TrendForum
created: '2012-01-21'
author: Jürgen
title: Micah Challenge
categories:
- Ontwikkelingssamenwerking
- Globalisering
- Maatschappij
---
[Christian Initiative](https://www.micahchallenge.org/) to halve poverty by 2015.
