---
source: TechSquare
created: '2014-09-06'
author: Jürgen
title: Center for Secure Design
categories:
- Baseline
- Privacy
- Security
- Architecture
---
The IEEE Computer Society [Center for Secure Design](https://cybersecurity.ieee.org/center-for-secure-design.html) intends to shift some of the focus in security from finding bugs to identifying common design flaws.
[Article](https://cybersecurity.ieee.org/center-for-secure-design/avoiding-the-top-10-security-flaws.html): Avoiding the Top 10 Security Flaws
