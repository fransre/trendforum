---
source: TrendForum
created: '2020-12-06'
author: Jürgen
title: Johnny Cash
categories:
- Maatschappij
- People
---
[Johnny Cash, The Last Great American (BBC TV Documentary 2004)](https://www.youtube.com/watch?v=NoCHxyxKBQM)

["JOHNNY CASH'S AMERICA" - (2008 Documentary)](https://www.youtube.com/watch?v=bsNyiS03jc0)

[The Gift: The Journey of Johnny Cash (Official Documentary)(2019, Youtube Original)](https://www.youtube.com/watch?v=5cMVNtmbwyo)

The second is like an extended version of the first, showing additionally Cindy Cash, next to Rosanne and John Carter Cash. Bob Dylan is prominent. Different aspects are used as organizing principle.
The third documentary is cinematographically the best using new HD video and old still pictures intermitted by old original video material. All current interviews are audio only, only giving the name of the speaker. Some extra low points of his career are shown, but not Hurt.
