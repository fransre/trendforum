---
source: Enkairoi
created: '2009-02-15'
author: Jürgen
title: Levine
categories:
- People
---

### Peter A. Levine

In this book [Waking the Tiger : Healing Trauma : The Innate Capacity to Transform Overwhelming Experiences](https://www.amazon.com/Waking-Tiger-Transform-Overwhelming-Experiences/dp/155643233X) Levine describes traumas as a bodily reaction which humans share with all mammals. In view of great danger a mammal has three possibilities of reaction: fight, flight and immobility.
He starts his book with the following paragraphs:

"A herd of impala gazes peacefully in a lush wadi. Suddenly, the wind shifts, carrying with it a new, but familiar scent. The impala sense danger in the air and become instantly tensed to a hair trigger of alertness. They sniff, look, and listen carefully for a few moments, but when no threat appears, the animals return to their grazing, relaxed yet vigilant.
Seizing the moment, a stalking cheetah leaps from its cover of dense shrubbery. As if it were one organism, the herd springs quickly toward a protective thicket at the wadi's edge. One young impala trips for a split second, then recovers. But it is too late. In a blur, the cheetah lungs toward its intended victim, and the chase is on at a blazing sixty to seventy miles an hour.\
At the moment of contact (or just before it), the young impala falls to the ground, surrendering to its impending death. Yet, it may be uninjured. The stone-still animal is not pretending to be dead. It has instinctively entered an altered state of consciousness shared by all mammals when death appears imminent. ...\
Physiologists call this altered state the "immobility" or "freezing" response."

The state of immobility protects the body from an unbearable amount of arousal. But since this happens when the body is in a high-energy state already, the energy has to be let gone. Animals (and often humans) do that after the dangerous moment. Only when the process of letting go is interrupted the energy is preserved in the body. It becomes a trauma. It can stay there unnoticed for years. Unconsciously, the person will try to reenact the situation by looking for similar or surrogate experiences to be able to complete the cycle of letting go the energy. Often that does not work and the patterns can become destructive to him/herself and others. Instead of re-enactment (also under psychological guidance) Levine recommends re-negotiation. If the cycle is not really completed during re-enactment the trauma can be worsened.

The [website](https://traumahealing.org/) of Peter A. Levine. An [introduction](https://www.psychologytoday.com/us/blog/the-body-knows-the-way-home/202005/why-you-cant-think-your-way-out-trauma) to his method and [interview](https://www.psychotherapy.net/interview/interview-peter-levine) with Peter.
