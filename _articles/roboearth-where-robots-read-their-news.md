---
source: TechSquare
created: '2014-01-15'
author: Jürgen
title: 'RoboEarth: where robots read their news'
categories:
- Devices
- Human Technical-System Interfacing
---
[RoboEarth](https://www.roboearth.org/what-is-roboearth) is a World Wide Web for robots.
