---
source: TrendForum
created: '2013-06-18'
author: Jürgen
lang: nl
title: Durf te vragen / Dare to ask
categories:
- Maatschappij
---
[DE KRACHT VAN SOCIALE OVERWAARDE](https://durftevragen.com/)

[A practical approach towards the sharing economy](https://daretoask.org/)
