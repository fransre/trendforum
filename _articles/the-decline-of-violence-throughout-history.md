---
source: TrendForum
created: '2011-09-16'
author: Willem
title: The decline of violence throughout history
categories:
- Geschiedenis
- Globalisering
- Maatschappij
---
Interesting statistics by Steven Pinker in this [TED talk](https://www.ted.com/talks/steven_pinker_on_the_myth_of_violence.html), where he argues that we are actually becoming more peaceful over millenia, centuries, decades and years.

great stuff,
about how all peoples left (one of the four) ancients pasts and joined global humanity (Mt.28:19-20).

The book: [Steven Pinker: The Better Angels of Our Nature: Why Violence Has Declined](https://www.amazon.com/Better-Angels-Our-Nature-Violence/dp/0670022950)

[Long video from edge](https://www.youtube.com/watch?v=MfYlSBbp0k4)

[Peter Singer reviews Steven's book](https://www.nytimes.com/2011/10/09/books/review/the-better-angels-of-our-nature-by-steven-pinker-book-review.html)
