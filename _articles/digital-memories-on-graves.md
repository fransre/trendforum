---
source: TrendForum
created: '2012-05-28'
author: Jürgen
title: Digital Memories On Graves
categories:
- Techniek
- Maatschappij
---
The process of burying the dead hasn't changed much over the centuries, but now their gravestones can provide a [digital link](https://www.npr.org/2011/05/30/136676964/technology-brings-digital-memories-to-grave-sites) to their life stories.
