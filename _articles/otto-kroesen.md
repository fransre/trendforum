---
source: Enkairoi
created: '2011-08-18'
author: Jürgen
title: Otto Kroesen
categories:
- People
---
Here the new book: [Leven in Organisaties](http://www.skandalon.nl/shop/theologie-cultuur/171-leven-in-organisaties.html) (Living in Organisations) and the accompanying website in [Dutch](http://temporavitae.nl/nl/) or [English](https://temporavitae.nl/).

His previous book is *Tegenwoordigheid van Geest in een Tijdperk van de Techniek* (The Presense of Spirit in an Age of Technology). Here is a [review](http://www.johanblaauw.nl/Recensies/reckroesen.htm) originally published in the Dutch newspaper Trouw.

Here the translation of the table of content:

### Otto Kroesen: The Presence of Spirit in the Era of Technology
1. Lost Communication
2. Lost Time
3. Re-Learning to Speak
4. Communication in a Technological Era
5. Winning Time
6. The Perspective of the History of Mankind
7. Origin and Future of the Technological Era

Detailed
1. Lost Communication
   - Introduction
   - The Law of Technology
   - Among Ethicists and Language of Technology
   - Subjectivism and Objective Science
   - Efficiency and Technology
   - Deficiency of Language
2. Lost Time
   - Language and Time
   - The Development of Industry
   -Quality of Work
3. Re-Learning to Speak
   - Language and Time
   - Speaking
   - Language as the Archetype of Society
   - Het afkoelingsproces van de taal
   - The Cross of Reality
   - Speech Problems of Society
4. Communication in a Technological Era
   - Introduction
   - Pluralism
   - The Doctrine of Social Movement
   - Imagination or Re-Present-Making
   - Beyond the Limited Technological Language
   - Beyond Abstract Ethical Guidelines
5. Winning Time
   - From Working Time to Eternity
   - Universal Human Five-Sphere Music
   - The Whole Human
   - Young: Living
   - Old: Love
   - Elder: Spirit
   - The Respiration of the Soul
   - Winning Time in the Industrial Era
6. The Perspective of the History of Mankind
   - Time as Learning Process
   - The Names of Historical Powers
   - History of Mankind
   - The Four Founders
7. Origin and Future of the Technological Era
   - Introduction
   - Origin of the Western Rationalism
   - Soul and World
   - Revolutions
   - The French Revolution
   - The Russian Revolution
   - The End of an Era
   - And now .... the Communication Era

An [webpage](https://www.tudelft.nl/tbm/over-de-faculteit/afdelingen/stafafdelingen/delft-centre-for-entrepreneurship/staff/personal-pages/jo-otto-kroesen) from TU Delft.
