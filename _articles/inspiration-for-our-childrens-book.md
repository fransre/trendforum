---
source: TrendForum
created: '2010-12-04'
author: Frans
title: Inspiration for our (children's) book
categories:
- Techniek
---
Another example of a difficult topic that has nevertheless been translated for the general public (or even children).

[Mommy, why is there a server in the house?](https://www.youtube.com/watch?v=YNHwgnpzY9w)

Very inspirational...

Let's use this simple-to-understand-approach and try to make a (Prezi) presentation first...
