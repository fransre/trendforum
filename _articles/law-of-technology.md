---
source: Enkairoi
created: '2011-01-30'
author: Jürgen
title: Law of Technology
categories:
---

Eugen Rosenstock-Huessy formulated a law of technology:
- each technological progress shortens time, expands space and destroys a social group.

originally formulated in German:
- „Jeder technische Fortschritt verkürzt die Zeit, erweitert den Raum, zerschlägt die Gruppe”
