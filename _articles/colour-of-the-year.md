---
source: TrendForum
created: '2013-01-05'
author: Jürgen
title: Colour of the year
categories:
- Maatschappij
---
The company [Pantone](https://en.wikipedia.org/wiki/Pantone) annually declares a particular colour as [Colour of the Year](https://www.latimes.com/features/home/la-lh-pantone-color-of-the-year-for-2013-emerald-20121205,0,4276579.story). This year it will be Emerald. Companies like Puma, Lufthansa, Starbuck’s, Prada, Milka or BIC use these colours for years in their products.
