---
source: TrendForum
created: '2011-04-12'
author: Jürgen
title: 'Announcement: Judaism Despite Christianity'
categories:
- Kerk
- Maatschappij
---
The 1916 [Wartime Correspondence](https://www.press.uchicago.edu/ucp/books/book/chicago/J/bo10388531.html) Between Eugen Rosenstock-Huessy and Franz Rosenzweig

Edited by Eugen Rosenstock-Huessy

With a new Foreword by Paul Mendes-Flohr and a new Introduction by Harold Stahmer

224 pages

Will Publish September 2011
