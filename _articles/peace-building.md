---
source: TrendForum
created: '2022-02-05'
author: Jürgen
title: Peace Building
categories:
- Maatschappij
- Globalisering
---

Two fundamental practioneers of the field

-----

### John Paul Lederach

![Conflict Transformation](https://www.researchgate.net/profile/Hillary-Musarurwa/publication/335910523/figure/fig4/AS:804757404930048@1568880525540/The-big-picture-of-conflict-transformation-Source-Lederach-2003.png)

John Paul Lederach:
[From Conflict Resolution to Strategic Peacebuilding](https://www.youtube.com/watch?v=LwsThUncRxE)
- “I have always said that the best thing that prepared me to deal with Somali war lords in Mogadishu was the initiatives I had to work through in congregational conflicts between pacifists, there is nothing worse in the world …”


-----

### Johan Galtung

- [Johan Galtung - en.wiki](https://en.wikipedia.org/wiki/Johan_Galtung)
- [Transcend](https://www.transcend.org/galtung/)
- Johan Galtung: [Transition From a Unipolar to a Multipolar Octagonal World](https://www.youtube.com/watch?v=9C2gzWOewpU)

-----
