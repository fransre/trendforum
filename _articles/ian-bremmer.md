---
source: TrendForum
created: '2023-06-18'
author: Jürgen
title: Ian Bremmer
categories:
- Maatschappij
- Globalisering
- People
---
![Ian Bremmer](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Ian_Bremmer_headshot.jpg/320px-Ian_Bremmer_headshot.jpg)

[Ian Bremmer](https://en.wikipedia.org/wiki/Ian_Bremmer) is the founder and president of [Eurasia Group](https://www.eurasiagroup.net), a political risk research and consulting firm. He is also a founder of [GZERO Media](https://www.youtube.com/@GZEROMedia/videos) <sup>[gZ](https://www.gzeromedia.com)</sup>, a digital media firm.\
Bremmer has published [11 books on global affairs](https://www.amazon.nl/s?k=ian+bremmer)\
[His analyses](https://www.gzeromedia.com/by-ian-bremmer/)

#### Exemplary statements
- update: 2-7-23 [Inside Putin’s Mind & What’s Next For Russia – Interview with Ian Bremmer](https://www.youtube.com/watch?v=-mWc3nuwl4U)
- [The Next Global Superpower Isn't Who You Think](https://www.youtube.com/watch?v=uiUPD-z9DTg) <sup>[gZ](https://www.gzeromedia.com/by-ian-bremmer/who-runs-the-world)</sup>
- [Henry Kissinger turns 100](https://www.youtube.com/watch?v=OgjKv6fdG00) <sup>[gZ](https://www.gzeromedia.com/quick-take/henry-kissinger-turns-100)</sup> [*transcript*]({{ '/ian-bremmer-kissinger-100' | relative_url }})
- [Putin Is Responsible For The Bloodshed In Ukraine](https://www.youtube.com/watch?v=483qOFwb2Hg) <sup>[gZ](https://www.gzeromedia.com/no-the-us-didnt-provoke-the-war-in-ukraine)</sup>
