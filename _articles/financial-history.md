---
source: TrendForum
created: '2011-11-04'
author: Jürgen
title: Financial History
categories:
- Geschiedenis
- Globalisering
---
Nial Ferguson: The Ascent of Money

* Documentary: [part 1](https://www.youtube.com/watch?v=QY8g_IsI_gY) and [part 2](https://www.youtube.com/watch?v=9RHVoDVOcHk)
* An [interview](https://www.youtube.com/watch?v=coItaKim-vQ) about The Ascent of Money

