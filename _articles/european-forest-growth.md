---
source: TrendForum
created: '2014-12-08'
author: Jürgen
title: European Forest Growth
categories:
- Geschiedenis
- Maatschappij
---
European Forest Growth [gif](https://www.wageningenur.nl/upload_mm/7/e/8/0e450a7a-7b59-4773-a0c5-4545324de2d1_Land_Changes_1900_2010_backwards.gif)

[from here](https://www.wageningenur.nl/en/Expertise-Services/Chair-groups/Environmental-Sciences/Laboratory-of-Geoinformation-Science-and-Remote-Sensing/Models/Hilda.htm)
