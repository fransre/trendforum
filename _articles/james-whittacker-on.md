---
source: TechSquare
created: '2012-07-11'
author: Jürgen
title: James Whittaker on
categories:
- Architecture
---
[leaving](https://blogs.msdn.com/b/jw_on_tech/archive/2012/03/13/why-i-left-google.aspx) Google,
[joining](https://blogs.msdn.com/b/jw_on_tech/archive/2012/03/14/why-i-joined-microsoft.aspx) Microsoft, and
[strategies](https://blogs.msdn.com/b/jw_on_tech/archive/2012/05/25/google-20-time-vs-the-microsoft-garage.aspx) for innovation by Google and Microsoft.

our New Era: [know and do](https://channel9.msdn.com/Events/ALM-Summit/ALM-Summit-3/A-New-Era-of-Computing)
which started in September 2012. The previous eras were compute-and-store, the 1990's owned by Microsoft, and search-and-browse, 2000's owned by Google.
