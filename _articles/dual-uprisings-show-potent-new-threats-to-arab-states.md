---
source: TrendForum
created: '2011-02-15'
author: Frans
title: Dual Uprisings Show Potent New Threats to Arab States
categories:
- Globalisering
- Maatschappij
---
An [article](https://www.nytimes.com/2011/02/14/world/middleeast/14egypt-tunisia-protests.html) about a pan-Arab youth movement dedicated to spreading democracy in a region without it. They were especially drawn to a Serbian youth movement called [Otpor](https://www.youtube.com/watch?v=bjb_fuvdzhU), which had helped topple the dictator Slobodan Milosevic by drawing on the ideas of an American political thinker, [Gene Sharp](https://www.aeinstein.org/organizations9173.html). The hallmark of Mr. Sharp’s work is well-tailored to Mr. Mubarak’s Egypt: He argues that [nonviolence is a singularly effective way to undermine police states](https://www.aeinstein.org/organizationsc5ed.html) that might cite violent resistance to justify repression in the name of stability.

[Mubarak ordered Tiananmen-style massacre of demonstrators, Army refused](https://www.americablog.com/2011/02/mubarak-ordered-tiananmen-style.html)

[The Arab uprisings of 2011 are like the European revolutions of 1848](https://www.slate.com/id/2285696/)

There seems to be a big discussion if Gene Sharp was really instrumental in the Egyptian uprising:

see:

* [Orientalising the Egyptian Uprising](https://www.jadaliyya.com/pages/index/1214/orientalising-the-egyptian-uprising)
* [The Americans Are Trying Hard To Take Credit For The Arab Spring](https://beirutspring.com/blog/2011/04/16/the-americans-are-trying-hard-to-take-credit-of-the-arab-spring/)
* [Gene Sharp: the New York Times story of the origins of the Tunisian and Egyptian uprisings ](https://angryarab.blogspot.com/2011/02/gene-sharp-new-york-times-story-of.html)
* [Why would you expect Nicholas Kristof to be any different: it is Gene Sharp again](https://angryarab.blogspot.com/2011/04/why-would-you-expect-nicholas-kristof.html)
