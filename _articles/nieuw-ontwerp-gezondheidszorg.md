---
source: TrendForum
created: '2010-12-13'
author: Jürgen
title: Nieuw ontwerp gezondheidszorg
categories:
- Overig
---
Gisteren over gesproken en vandaag al de aanleiding in de krant: [Medisch specialisten doen te veel onnodige operaties](https://www.nrc.nl/binnenland/article2648834.ece/Medisch_specialisten_doen_te_veel_onnodige_operaties).
De incentive structuur klopt niet: diagnose en behandelaar moeten gescheiden worden. Tussen behandelaars kan marktwerking gebruikt worden. Hoe staat het met de diagnosten?
