---
source: TrendForum
created: '2011-10-23'
author: Jürgen
title: Policies often produce disappointing results
categories:
- Maatschappij
---
[About](https://www.nytimes.com/2011/07/08/opinion/08brooks.html) the consequences of the complexity of human nature:

"Poorer people have to think hard about a million things that affluent people don’t."
