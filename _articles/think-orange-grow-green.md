---
source: TrendForum
created: '2014-02-12'
author: Jürgen
title: Think Orange Grow Green
categories:
- Ontwikkelingssamenwerking
- Globalisering
---
A [cluster](https://www.nethwork.info/) of 17 Dutch horticultural companies (including HAS university of applied science) that supports the sustainable development of the horticultural market in Central America.

Looks like a network approach to international business!
