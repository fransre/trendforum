---
source: TrendForum
created: '2013-07-15'
author: Jürgen
title: Relational approach to economics
categories:
- Maatschappij
- Financiën
---
Michael Schluter explicates a [relational approach to economics](https://www.youtube.com/watch?v=Si9_FX8-ZHY) based on biblical ideas and what such an approach means for Europe.

Tomas Sedlacek talks about the [ethics of economics](https://www.youtube.com/watch?v=VR-3MBd_sLQ) and how its perceived absence confuses the economic discussion.

An [interview](https://www.spiegel.de/international/business/spiegel-interview-with-tomas-sedlacek-greed-is-the-beginning-of-everything-a-822981-druck.html) by Tomas Sedlacek: 'Greed is the Beginning of Everything'
His [book](https://www.amazon.com/Economics-Good-Evil-Gilgamesh-ebook/dp/B0058C6Q8U): Economics of Good and Evil: The Quest for Economic Meaning from Gilgamesh to Wall Street

After Capitalism: Rethinking Economic Relationships
[book](https://www.jubilee-centre.org/resources/after_capitalism_rethinking_economic_relationships) by Paul Mills and Michael Schluter, May 2012

"The problems of economics are not of a mathematical nature - and so cannot be cured by mathematics. It is the philosophy, the questions of the soul, that must be addressed. This book offers a fine immersion in exactly that."
Dr Tomas Sedlacek - Member of the Czech Republic's National Economic Council and former adviser to Vaclav Havel

In this video: "Transform Capitalism from within" Michael explicates [his analysis](https://www.youtube.com/watch?v=HQNeDLPyXiI) of our current economic situation and mentions the principles for a relationship-based economy with new institutions, or download the [book](https://www.relationshipsglobal.net/Web/Content/Default.aspx?Content=43)
