---
source: TrendForum
created: '2024-01-04'
author: Jürgen
title: "Ian Bremmer: Henry Kissinger turns 100"
categories:
- Maatschappij
- Globalisering
- People
---
[Henry Kissinger turns 100](https://www.youtube.com/watch?v=OgjKv6fdG00) <sup>[gZ](https://www.gzeromedia.com/quick-take/henry-kissinger-turns-100)</sup>

Transkript:

Hi, everybody. Ian Bremmer here.\
Happy Tuesday to you after Memorial Day weekend,  and I thought I'd talk for a bit about Dr. Kissinger since he's just turned 100 years old.

I'm pretty sure he's the only centenarian that I know well,  and lots of people have spoken their piece  about how much they think he's an amazing diplomat, unique,  and how much they think he's a war criminal, unique,  and maybe not surprising to anyone.
I'm a little bit in between those views.  I have known him for a long time.  I remember first time I met him was around 1994.

I'd just come back from Ukraine,  and I introduced myself to him at some event in New York,  and he was interested in what I had to say,  and said, Why don't you come and have lunch with me?"
Which was kind of surprising, since he didn't know me at all, and I thought, well, may be he's just getting rid of me to talk to other people that are in line, but couple days later, I find myself in his office having tea sandwiches and talking about Ukraine and the context of Russia relations,  Europe relations and the US.

It could have been with a professor of mine, you know, or some colleague, the kind of discussion we were having. It didn't feel like he was being pompous or talking down to me. He talked like, spoke like he wanted to understand what I had learned from my relationships on the ground  and my analysis, and challenge it against his own, so that was pretty interesting.

Of course, I will tell you at that point, the reading that I had done of Kissinger was mostly in his own words on diplomacy  and from some professors of mine at Stanford and people, the colleagues there that had that generally were very well disposed to him.

Since then, I probably sit down with him a few times a year and talk about global issues, and it's always interesting to hear his perspective. I would say that when it comes to broad international relations, he is, you know, of a very specific view and school, very transactional, very strategic.

He's also of a certain time and place in the sense that he still doesn't believe that Europe really matters. Doesn't get, doesn't accept that the European Union has become much stronger, much more capable as an institution than it was 10, 20, 30 years ago, than it was when he was saying,"Who do I call in Europe? "Give me a phone number.They don't have one."

On the other hand, he's retooled himself considerably to truly learn about and understand artificial intelligence, and not just from a layman's perspective, but understand the policy implications, and to do that at the age of100 is pretty extraordinary. I consider AI to be an utter game-changer geopolitically, more important than any transformation I've seen on the global stage since I did my PhD some 30-plus years ago, but for Kissinger to do that at 100 is quite something, and the fact that he has the wherewithal and the acumen to do that, I'm sure, says a lot about why he still is put together as he is.

There was an event that I did for the Young Presidents' Organization, a few thousand folks a few months ago, and this was on a big stage, and Kissinger was going to give a master class, but they needed someone to engage with him for an hour, and he asked if I'd do it, so I said, "Sure," and what was interesting about it was, I mean, you know, I sat in close to him so that he could hear everything I was saying clearly, but, I mean, for an hour this was a very serious conversation, frankly, as good as anyone else I've spoken with on my show over the course of the last several years, and again, doing that at 100, so, I mean, put all of that together, you have to be impressed in the sense that it makes an impression upon you, whether negative or positive.

It's an impression that someone can do that at his age, so that's all of the, my relationship with him, and when I disagree with him, I say so,  and do it more strongly privately than I do it publicly, in part because his willingness to respond to that usefully publicly is fairly limited, and so you don't get value out of it, but that doesn't mean that I'm a big proponent of his worldview, and some of that is true today.

A lot more of that is true, of course, historically.  You learn a lot about someone by what they do when they're in a position to really do something, when they're in a position of power, when it matters, I mean, you know, for example, I mean, you know, when I'd like to believe that when the chips were down, and I had the ability to either keep my mouth shut or say something publicly about Elon Musk, and it would've been a lot more convenient to do the shutting your mouth, that I use my platform, hopefully, to make a more positive difference, and I think that that's, in a very small way. In a very big way, of course, when you're national security advisor, secretary of state, you have real power in your hands, and you make decisions that destroy people.

That says a lot about who you are, and I obviously can't, in any way, justify or support or align myself with a lot of the decisions that Kissinger has taken, and that, you know, you look at Chile and the support for Pinochet and the coup, overthrowing a democratically elected government, something that he strongly and individually supported despite lots of opposition inside the Nixon administration and from President Nixon at the time.  I think about Kissinger's support for Suharto in Indonesia and the killing in East Timor, over 100,000 innocent civilians dead from, you know, what now has an independent country, but at the time, was the Americans happy to privately support, you know, essentially, a genocide, and that was a Kissinger policy.
This was not American exceptionalism; it was the the opposite of that, and, you know, Vietnam, a lot of people take responsibility for the murderous interventions in Vietnam, and not something that the Americans learned enough from, but specifically, around Cambodia, and a bombing campaign that was conducted in secret, which was denied for along time by Kissinger, and again, over 100,000 civilians dead, and then, one of the most murderous regimes we've ever seen in the20th century comes in, the Khmer Rouge, because the country had been so destabilized by the Americans and by Kissinger's decision that led to the deaths of millions more.

So, you know, that's on his hands individually, and, you know, I guess you live to be 100, and you have that kind of power. Few people are going to be proud of everything they did, but this is a very different kind of decision, and I mean, you know, it's,I will say that, you know, part of meeting Kissinger turned me off from power.

The fact that someone who had been a Harvard professor, who I respected so much from the writings that I read of his, and then as you learn what that person, despite what they're like when they meet you, had done when they were in power, just turns you off from power. It makes you feel like that's something you don't want to be any part of.

That was my initial sort of knee-jerk reaction. I think it's become more nuanced since then, but you can't have a retrospective about one of the men that has had the greatest impact on American diplomacy and its influence around the world without recognizing just how negative some of that has been, and I will say that in today's very polarized environment, you know, Kissinger is one of the people probably most responsible for the fact that when people around the world see that you're an American and do international affairs, they assume that your views of the world are equally, you know, high-handed, hypocritical, disdainful of the rights of human beings as humans. I mean, in some fundamental way, I'm probably the antithesis of realpolitik because I actually believe, first and foremost, that if there are 8 billion people on the planet, they all kind of count the same, and the fact that that means more to me than any individual citizenship is pretty much not any of what Kissinger did when he was secretary of state, which is kind of sad for someone that's that bright and someone that has the capacity to do so much more.

So that's my view of Kissinger at 100, probably a little different than a lot of what you've heard thus far on the topic, but for Memorial Day, in particular,
 maybe a more appropriate read. So that's it for me. I hope everyone's well, and take it easy this week.

 [*back to* Ian Bremmer]({{ '/ian-bremmer' | relative_url }})
