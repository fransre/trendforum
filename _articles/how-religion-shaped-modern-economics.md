---
source: TrendForum
created: '2021-01-21'
author: Frans
title: How Religion Shaped Modern Economics
categories:
- Financiën
- Maatschappij
---
*In the 18th century, a new Protestant belief that people have control over their destinies fostered the rise of free-market ideas.*

The conventional account of modern thinking about economics starts with Adam Smith’s 1776 book “The Wealth of Nations.” Smith’s insights, in turn, are usually said to be a product of the secularism of the Enlightenment—a historic turn from thinking in terms of a God-centered universe toward ideals of human rationality and self-sufficiency.

But that story is seriously incomplete. In fact, our modern Western understanding of market competition as the key to economic progress owes a great deal to religion—specifically, the new ideas that emerged in the English-speaking Protestant world in the late 17th and the 18th century. Critics of capitalism sometimes complain that the belief in competitive markets, among economists and many ordinary citizens too, is a form of religion. There is something to the idea, though not in the way the critics mean.

The idea that there is a connection between religion and capitalism isn’t new. The German sociologist Max Weber famously argued that religious belief had been an important spur to the growth of capitalism. Weber pointed in particular to the Calvinist notion of predestination—the belief that God decided whether each individual would be saved or damned before the person was even born. Driven by anxiety over their ultimate fate, Weber theorized, believers sought to convince themselves that they were among God’s “elect” by displaying virtues like thrift, industriousness and individual initiative. In this way, a “Protestant ethic” emerged that enabled the rise of modern capitalism.

Belief in predestination hindered the emergence of modern capitalism’s key idea—that human beings can rationally advance their own and others’ economic condition.

For purposes of economic thinking, however, Weber had things exactly backward. Belief in predestination actually hindered the emergence of modern capitalism’s key idea—that human beings can rationally advance their own and others’ economic condition. After all, if it was impossible for individuals to make any choice or take any action to promote their ultimate spiritual prospects, how could they make choices or take action to improve their well-being in this world?

By Adam Smith’s time, however, Protestant thought had turned away from the idea of predestination. The English and Scottish clergy began to preach instead that men and women are endowed with reason and that human character is inherently good. John Tillotson, the Archbishop of Canterbury in the late 17th century, argued that far from being predestined to heaven or hell, people can “cooperate” with God in effecting their salvation. As the philosopher John Locke put it in a famous metaphor, people have “the candle of the Lord” by which to see and then act.

The idea that we are morally conscious agents, with free will and the power of choice, had important implications for the economic sphere as well. This more expansive and more optimistic vision of human character helped to foster new insights into the beneficial consequences of individual initiative. If people can understand what is in their spiritual interest and act on it, they can do the same for their worldly interests, improving their own lives and the lives of others, too. Adam Smith’s crucial contribution was to identify market competition as the mechanism that enables individual initiative to realize its potential.

This benign sense of our human capacities, enabled by a historic transition in religious thinking, has continued to influence the trajectory of Western economics ever since. It also helped to inspire the new experiment in political democracy that Americans began the same year that Smith published “The Wealth of Nations.”

Importantly, none of this came about as a result of religiously committed individuals deliberately bringing their personal beliefs to bear on their economic or political thinking. Adam Smith was at most a deist, like Benjamin Franklin and Thomas Jefferson. When he took up his professorship at the University of Glasgow, he asked to be exempted from the requirement to begin each lecture with a prayer (his request was denied). Smith’s close friend David Hume, a philosopher and a major contributor to the early development of economics, was an avowed skeptic and an outspoken opponent of organized religion; he notoriously referred to Church of England bishops as “Retainers to Superstition.”

In the 18th century, intellectual life was more integrated. Theology was discussed in the same circles as the sciences and humanities.
But Smith and Hume lived at a time when religion was more central and more pervasive than it is in today’s Western world. Intellectual life was more integrated too. Theology was discussed in the same circles as the sciences and humanities. The religious ideas that these proto-economists read about and discussed influenced the economic ideas they produced, just as the ideas of today’s economists are shaped by what we learn from physicists, demographers and virologists.

The more optimistic view of human nature fostered by the religious thinking of the 18th century, just when modern Western economics was taking shape, continues to shape how 21st-century Americans view the most contested public policy issues of our time. Americans still have a profound faith that our individual destinies are in our own hands. That’s why many people who have only the remotest prospect of ever making their way into the top income-tax bracket nonetheless favor keeping that tax rate low, and why people who have no chance of inheriting money from a taxable estate passionately oppose “death taxes.”

In broader terms as well, our economic thinking reflects its optimistic religious origins. The respect we attach to economic self-improvement as an expression of political liberty; our commitment to economic development on the national and even world scale; our belief that economic progress leads to moral progress too; above all, our trust in competitive markets as a way to harness individual economic energies for the broader good—all of these bedrock principles show that the influence of religious thinking on American capitalism isn’t just historical but ongoing.

[Article](https://www.wsj.com/articles/how-religion-shaped-modern-economics-11610643698) by [Benjamin M. Friedman](https://en.wikipedia.org/wiki/Benjamin_M._Friedman)
