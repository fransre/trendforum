---
source: TrendForum
created: '2022-07-10'
author: Jürgen
title: ERHFund Essay Competition
categories:
- Maatschappij
---
The Eugen Rosenstock-Huessy Fund announces its [first essay competition](https://www.erhfund.org/gritli-not-chosen/2022-essay-competition/):

#### “Into Life”: Rethinking the Legacy of Franz Rosenzweig and Eugen Rosenstock-Huessy
