---
source: TechSquare
created: '2018-02-19'
author: Jürgen
title: New Adobe Character Animator SW
categories:
- Devices
- Human Technical-System Interfacing
---
How [Adobe Character Animator](https://www.youtube.com/watch?v=PD9-zrGxrs4) in After Effects CC helps them break new ground in late night television.
