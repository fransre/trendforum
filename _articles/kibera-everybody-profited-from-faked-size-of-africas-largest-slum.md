---
source: TrendForum
created: '2010-11-24'
author: Jürgen
title: 'Kibera: Everybody profited from faked size of "Africa''s largest slum" '
categories:
- Ontwikkelingssamenwerking
- Globalisering
---
Fast update of the english wikipedia: [Kibera](https://en.wikipedia.org/wiki/Kibera)
"The 2009 Kenya Population and Housing Census reports Kibera's population as 170,070, contrary to previous estimates of one or two million people."

Dutch wikipedia: [Kibera](https://nl.wikipedia.org/wiki/Kibera) still reports the old numbers
[Der Millionenschwindel](https://www.zdf.de/ZDFde/inhalt/14/0,1872,8144110,00.html)
eng. wikipedia:
"Kibera is one of the most studied slums in Africa, not only because it sits in the centre of the modern city, but also because UN-HABITAT, the United Nations' agency for human settlements, is headquartered close by."

Could there be more productive incentive structures?

Het blijft een beetje een raar verhaal dat ontwikkelingsorganisaties, ziekenhuizen, de VN, etc het veel te hoge inwonertal hebben "verzonnen". DE bron is misschien niet meer te achterhalen, maar er moet toch meer over te zeggen zijn dan alleen het feit dat die te hoge getallen gebruikt zijn...

Important insight:

[Riz Khan interviewing Linda Polman: Rethinking humanitarian aid](https://www.youtube.com/watch?v=TC7M_g0foJk)

NGOs are in competition, are dependent on their donors and are supporting the donor countries political agenda.

Another critical [story](https://www.spiegel.de/international/world/0,1518,druck-718656,00.html) from Afghanistan. Polman is also cited.
