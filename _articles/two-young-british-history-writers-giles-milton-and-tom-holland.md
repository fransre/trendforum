---
source: TrendForum
created: '2011-01-29'
author: Jürgen
title: 'Two young British history writers: Giles Milton and Tom Holland'
categories:
- Geschiedenis
---
[Giles Milton](https://en.wikipedia.org/wiki/Giles_Milton) specializes in the history of exploration

+ The Riddle and the Knight: In Search of Sir John Mandeville (1996)
+ Nathaniel's Nutmeg: How One Man's Courage Changed the Course of History (1999) about the Dutch
+ Big Chief Elizabeth: The Adventures and Fate of the First English Colonists in America (2000)
+ Samurai William: The Englishman Who Opened Japan (2002)
+ White Gold: The Extraordinary Story of Thomas Pellow and North Africa's One Million European Slaves (2005)
+ Paradise Lost: Smyrna 1922 (2008)
+ Wolfram: The Boy Who Went To War (2011)

and [Tom Holland](https://en.wikipedia.org/wiki/Tom_Holland_%28author%29) writes about empires of history:

+ Rubicon: The Last Years of the Roman Republic (2003)
+ Persian Fire: The First World Empire and the Battle for the West (2005)
+ Millennium: The End of the World and the Forging of Christendom (2008)

Here a third one: [Andrew Robinson](https://en.wikipedia.org/wiki/W._Andrew_Robinson) writes about history of science and India:

+ The Coasts of India together with Ashvin Mehta (1987)
+ Maharaja: The Spectacular Heritage of Princely India together with Sumio Uchiyama (1988)
+ Satyajit Ray: The Inner Eye (1989)
+ The Art of Rabindranath Tagore (1989)
+ The Shape of the World together with Simon Berthon (1991)
+ Earth Shock: Hurricanes, Volcanoes, Earthquakes, Tornadoes & Other Forces of Nature (1993)
+ Rabindranath Tagore: The Myriad-minded Man together with Krishna Dutta (1995)
+ Rabindranath Tagore: An Anthology together with Krishna Dutta and Rabindranath Tagore (1997)
+ Selected Letters of Rabindranath Tagore together with Krishna Dutta and Rabindranath Tagore (1997)
+ The Story of Writing: Alphabets, Hieroglyphs and Pictograms (2000)
+ The Man Who Deciphered Linear B: The Story of Michael Ventris (2002)
+ Lost Languages: The Enigma of the World's Great Undeciphered Scripts (2002)
+ Satyajit Ray, The Inner Eye: The Biography of a Master Film-Maker (2003)
+ Satyajit Ray: A Vision of Cinema (2005)
+ Einstein: A Hundred Years of Relativity (2005)
+ The Last Man Who Knew Everything: Thomas Young, The Anonymous Polymath Who Proved Newton Wrong, Explained How We See, Cured the Sick, and Deciphered the Rosetta Stone, Among Other Feats of Genius (2006)
+ The Story of Measurement (2007)
+ Writing and Script (2009)
