---
source: TrendForum
created: '2012-10-16'
author: Jürgen
title: Rethinking How We Teach Economics
categories:
- Wetenschap
- Financiën
---
NY Times started a series: [Rethinking How We Teach Economics](https://www.nytimes.com/roomfordebate/2012/04/01/how-to-teach-economics-after-the-financial-crisis)

The suggestion of [Nassim Taleb](https://en.wikipedia.org/wiki/Nassim_taleb) was to [Throw Out the Probability Models](https://www.nytimes.com/roomfordebate/2012/04/01/how-to-teach-economics-after-the-financial-crisis/throw-out-the-old-economic-models)

More info about Nassim:
[@Eonomist](https://www.economist.com/topics/nassim-nicholas-taleb)
[Homepage](https://www.fooledbyrandomness.com/)
[Taleb distribution](https://en.wikipedia.org/wiki/Taleb_distribution)
