---
source: TrendForum
created: '2021-12-05'
author: Frans
title: The Historical Context of the Bible
categories:
- Geschiedenis
- Kerk
- Maatschappij
---
This is a series of 37 lectures covering the ancient historical setting of the Bible.
The instructor is [Bruce W. Gore M.A.](https://www.brucegore.com), J.D., adjunct professor of biblical studies at Whitworth University, Spokane.
The lecture was presented to a group of adult students at First Presbyterian Church, Spokane,
and the bias of the teacher is generally sympathetic to the biblical text,
while allowing at the same time for problem areas in the effort to correlate biblical data
with what is otherwise generally accepted from known ancient historical sources.

1. [Introduction to the Historical Context of the Bible](https://www.youtube.com/watch?v=L-nhOwEwtrE&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
2. [Genesis 1 and Enuma Elish](https://www.youtube.com/watch?v=QGT4GghrTY0&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
3. [Noah's Flood and the Epic of Gilgamesh](https://www.youtube.com/watch?v=UrhpNRFFfJ8&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
4. [Abraham in Historical Context](https://www.youtube.com/watch?v=N18oqa1lQkw&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
5. [Moses and the Code of Hammurabi](https://www.youtube.com/watch?v=eWXbt667Y6U&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
6. [The Adventures of Abraham in Egypt](https://www.youtube.com/watch?v=npInIbHGlSA&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
7. [Joseph and the Hyksos Pharaohs](https://www.youtube.com/watch?v=0vKRa6BNOdc&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
8. [Exodus and the 18th Dynasty](https://www.youtube.com/watch?v=iYZF09T5Klk&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
9. [Egypt and the Era of the Israelite Judges](https://www.youtube.com/watch?v=mMZ2Rw4Rig4&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
10. [The Hittites and the Era of the Israelite Judges](https://www.youtube.com/watch?v=kqIz9vjjtDs&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
11. [The Assyrian Empire and the Israelite Monarchy](https://www.youtube.com/watch?v=i7rSCsDBPZU&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
12. [The Assyrian Empire and Jonah](https://www.youtube.com/watch?v=3FZECx74Dmg&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
13. [The Assyrian Empire, Isaiah and King Ahaz](https://www.youtube.com/watch?v=4q-I8fSop7c&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
14. [Hezekiah, Sennacherib, and Big Surprises](https://www.youtube.com/watch?v=-O5RHbq3BLI&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
15. [Manasseh and the End of the Assyrian Empire](https://www.youtube.com/watch?v=0iXFsA8gySE&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
16. [Assyria Falls, Babylon Rises, and Josiah Reforms](https://www.youtube.com/watch?v=kyyC7btuHAg&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
17. [Nebuchadnezzar and Jeremiah's Letter to the Exiles](https://www.youtube.com/watch?v=LT3R32CSgW4&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
18. [Jehoiachin, Belshazzar, and the Fall of Babylon](https://www.youtube.com/watch?v=XyyS6_700-8&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
19. [Cyrus and the Liberation of God's People](https://www.youtube.com/watch?v=N1zCyzn2-wU&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
20. [Darius and the Completion of the Second Temple](https://www.youtube.com/watch?v=L8jnAfvYTFc&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
21. [Xerxes the Great and Queen Esther](https://www.youtube.com/watch?v=zKKaonre_Jk&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
22. [Artaxerxes, Ezra, and Nehemiah](https://www.youtube.com/watch?v=7OOkYjCdx-k&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
23. [The Greeks Seek for Wisdom](https://www.youtube.com/watch?v=upSGIRhAQRc&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
24. [Alexander the Great and the Old Testament](https://www.youtube.com/watch?v=q3KjXB-1oA8&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
25. [The Hellenistic Age: Alexander to Antiochus III](https://www.youtube.com/watch?v=2SUTgXQUGdQ&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
26. [Antiochus Epiphanes and the Maccabees](https://www.youtube.com/watch?v=6hwkThHYBXs&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
27. [The Roman Empire and Nebuchadnezzar's Vision](https://www.youtube.com/watch?v=RUMlM5kPkkE&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
28. [Lessons from Rome's Seven Kings](https://www.youtube.com/watch?v=FQR6WwvLwL0&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
29. [The Rise of the Roman Republic](https://www.youtube.com/watch?v=JlCYWHxDzJI&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
30. [Rome and Israel Collide](https://www.youtube.com/watch?v=S3VJ37dkZ6w&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
31. [Augustus Caesar and Imperial Rome](https://www.youtube.com/watch?v=E4pkKtDMJo8&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
32. [Herod the Great](https://www.youtube.com/watch?v=5QJaw7HK6sg&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
33. [Tiberius and Christian Beginnings](https://www.youtube.com/watch?v=3k44RP8vl24&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
34. [Caligula, Agrippa, and a Sermon to Cornelius](https://www.youtube.com/watch?v=hQRWJvKXe5E&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
35. [Claudius and the Journeys of Paul](https://www.youtube.com/watch?v=-SZSBPo4KZI&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
36. [Nero and Imperial Persecution of Christians](https://www.youtube.com/watch?v=RqZhch0LKlQ&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
37. [The Fall of Jerusalem and the Apocalypse](https://www.youtube.com/watch?v=kJVZTFuyAXE&amp;list=PLYFBLkHop2alFacrvkn2qtR3y1D2fQmad)
