---
source: TrendForum
created: '2010-11-18'
author: Jürgen
title: Paper about Rosenstock-Huessy's philosophy of history
categories:
- Geschiedenis
- Kerk
- Globalisering
---
[Michael McDuffee: A Re-reading of Rosenstock-Huessy's Philosophy of History in the Era of God's Return](https://www.lsu.edu/artsci/groups/voegelin/society/2010%20Papers/Michael%20McDuffee.pdf)

Interesting article, more a list of quotations than a systematic exposition.

Abstract:

This paper examines the value of Eugen Rosenstock-Huessy's philosophy of history in the present post-secular, global age. Rosenstock-Huessy anticipated that although we could no longer speak to one another from either an exclusively religious or secular point of view, we must nonetheless speak from out of salvational history, or Heilsgeschichte, the history we really live. Because of Jesus coming into history we all live in one another’s lives and can adapt one another’s habits and choices of personhood. Beginning in a European context, what Rosenstock-Huessy described as the revolutionary state of mutual dependence and moral interplay now defines our post-secular global living quarters. However in this new setting, the lifestyle we bequeath to our children threatens future generations to suffer decadence, the condition of being trapped in the present without a calling to be co-founders of the future. To overcome this crisis we cannot follow the secular sciences because, as he put it, they "presume that the future is not created but caused, the past not looked upon as an authority but as mere tyrannical cultural lag, the mind not experienced as brother fellowship but as a blueprint, and the earth not experienced as waiting to be led to its perfection but as an objective obstacle to be crushed or exploited." In the present new setting beyond what we had once been we can choose to become what Rosenstock-Huessy called men and women of good will, those who believing in the unity of man struggle to forge temporary truces today.

[Rosenstock-Huessy’s Theopolitical Reading of Islam in the Postsecular Era](https://mcduffee.wordpress.com/2012/10/24/rosenstock-huessys-theopolitical-reading-of-islam-in-the-postsecular-era/)
