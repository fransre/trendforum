---
source: TrendForum
created: '2013-03-29'
author: Frans
title: Want to Help People? Just Give Them Money
categories:
- Ontwikkelingssamenwerking
- Financiën
---
[Harvard Business Review](https://blogs.hbr.org/cs/2013/03/want_to_help_people_just_give.html) writes that [GiveDirectly](https://www.givedirectly.org)'s [rigorous data](https://www.givedirectly.org/evidence.php) shows that no-strings-attached cash transfers improve health and downstream financial gains.

The data fights conventional wisdom: Money spent on alcohol and cigarettes either decreases, stays constant or increases in the same proportion as total other expenses.

And they have a new concept: What if cash transfers to the poor are used as a standard benchmark against which to measure all development aid? What if every nonprofit that focused on poverty alleviation had to prove they could do more for the poor with a dollar than the poor could do for themselves?
