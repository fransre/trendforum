---
source: TrendForum
created: '2015-09-27'
author: Jürgen
title: Stephen Colbert looks back
categories:
- Kerk
- Maatschappij
---
Stephan is [interviewed by](https://www.youtube.com/watch?v=lF5tudIqN7w) a catholic priest and talks about the Catholic Church and the Pope, the Bible (his favorite verse) and Jesus, C.S. Lewis and relates them to humor, satire and joy.
