---
source: TechSquare
created: '2014-02-12'
author: Jürgen
title: The Hague Security Delta
categories:
- Security
---
Aankomende donderdag gaat de The Hague Security Delta (HSD), een samenwerking tussen bedrijven, kennisinstellingen en overheden, officieel van start met de opening van haar campus. Deze omvat kantoor- en ontmoetingsruimtes en diverse labs gericht op verschillende aspecten van veiligheid, met een belangrijk aandeel voor computerbeveiliging en nadruk op nieuwe technologie. Ook vormt de campus de thuisbasis voor Cyber Security Academy, een opleidingsinstituut van de Universiteit Leiden, de TU Delft en de Haagse Hogeschool.

De HSD is een initiatief van onder meer TNO, Fox-IT, Siemens Nederland, Thales Nederland, Capgemini, de Haagse Hogeschool en het ministerie van Veiligheid en Justitie. Daarnaast hebben zich enkele tientallen partners verbonden aan het initiatief.

De Cyber Security Academy (CSA) richt zich op kennisontwikkeling voor hoger opgeleide professionals op het terrein van cybersecurity. De kern wordt een nieuwe masteropleiding, die vanaf 1 september van start gaat. De opleiding is gericht op technische professionals, juridische specialisten en beleidsmakers en duurt in deeltijd 1,5 tot 2 jaar.

!rewrite!
