---
source: Enkairoi
created: '2011-08-18'
author: Jürgen
title: Vishal Mangawadi
categories:
- People
---

Vishal Mangalwadi (1949-) is an international lecturer, social reformer, cultural and political columnist, and author of thirteen books. Born and raised in India, he studied philosophy at universities, in Hindu ashrams, and at L’Abri Fellowship in Switzerland. In 1976 he turned down several job offers in the West to return to India where he and his wife, Ruth, founded a community to serve the rural poor. Vishal continued his involvement in community development serving at the headquarters of two national political parties, where he worked for the empowerment and liberation of peasants and the lower castes.

## Vishal Mangalwadi: The Book that Made Your World

### How the Bible Created the Soul of Western Civilization [Amazon](https://www.amazon.com/Book-that-Made-Your-World/dp/1595553223)

Discover how the Bible became the West's source of human rights, justice, heroism, optimism, compassion, capitalism, family, and morality.

In the 1960s many from the West went to the East in search of spiritual wisdom. The Book That Made Your World reverses the journey. Vishal Mangalwadi, an Indian philosopher, takes readers on a historical journey through the last millennium, exploring why and how the Bible reformed Europe and made the West a uniquely thinking civilization: technical and tolerant, scientific and free, just and prosperous. Readers will learn:
- Why an American president puts his hand on the Bible to take the oath of a secular office
- What forced British monarchs from Henry VIII to James I to submit to the Bible's authority
- Why Bible translators Wycliffe, Luther, and Tyndale became history's greatest revolutionaries
- How the Bible globalized western education

### Table of Contents:
*Forword* by J. Stanley Mattson, Ph.D.\
*Prologue: Why This Journey into the Sould of the Modern World?*\
- Part I: The Soul of Western Civilization\
  - 1 The West Without Its Soul: From Bach to Cobain

- Part II: A Personal Pilgrimage:
  - 2 Service: Or a Ticket to Jail?
  - 3 Quest: Can Blind Man Know the Elephant?
  - 4 Self: Am I Like Dog or God?

- Part III: The Seeds of Western Civilization
  - 5 Humanity: What is the West's Greatest Discovery?
  - 6 Rationality: What Made the West a Thinking Civilization?
  - 7 Technology: Why did the Monks Develop It?

- Part IV: The Millenium's Revolution
  - 8 Heroism: How Did a Defeated Messiah Conquer Rome?
  - 9 Revolution: What Made Translators World Changers?

- Part V: The Intellectual Revolution
  - 10 Languages: How Was Intellectual Power Democratized?
  - 11 Literature: Why Did Pilgrims Build Nations?
  - 12 University: Why Educate Your Subjects?
  - 13 Science: What Is Its Source?

- Part VI: What Made the West the Best?
  - 14 Morality: Why Are Some Less Corrupt?
  - 15 Family: Why Did America Surge Ahead of Europe?
  - 16 Compassion: Why Did Caring Become Medical Commitment?
  - 17 true Wealth: How Did Stewardship Become Spirituality?
  - 18 Liberty: Why Did Fundamentalism Produce Freedom?

- Part VII: Globalizing Modernity
  - 19 Mission: Can Stone Age tribes Help Globalization?
  - 20 The Future: Must The Sun set on the West?

Appendix: The Bible: Is It a Fax From Heaven?\
Notes\
With Gratitude\
About the Author\
Index

Praise of The Book that Made Your World:
- "This is an extremely significant piece of work with huge global implications. Vishal brings a timely message." (Ravi Zacharias, author, Walking from East to West and Beyond Opinion)
- "In polite society, the mere mention of the Bible often introduces a certain measure of anxiety. A serious discussion on the Bible can bring outright contempt. Therefore, it is most refreshing to encounter this engaging and informed assessment of the Bible's profound impact on the modern world. Where Bloom laments the closing of the American mind, Mangalwadi brings a refreshing optimism." (Stanley Mattson, founder and president, C. S. Lewis Foundation)
- "Vishal Mangalwadi recounts history in very broad strokes, always using his cross-cultural perspectives for highlighting the many benefits of biblical principles in shaping civilization." (George Marsden, professor, University of Notre Dame; author, Fundamentalism and American Culture)
- "The Indian perspective is a breath of fascinating fresh air for American readers. I wish and pray that it finds readers willing to have their minds shaken and their hearts, yes, their hearts, stirred as well." (James W. Sire, author, The Universe Next Door and Habits of the Mind)
- "This book is well overdue! If there is one book that has shaped Europe's art, architecture, commerce, education, ethics, family life, freedom, government, healthcare, law, language, literature, music, politics, science, social reform, and much more, it is the Bible. Yet biblical illiteracy is almost universal in Europe today. We need Vishal's clear, prophetic, Eastern voice to jolt us back to reality before our rich biblical heritage slips beyond our grasp." (Jeff Fountain, director, Schuman Centre for European Studies, the Netherlands)
- "Vishal's book is one of a kind-vast in scope, penetrating in its depth, and prophetic in its message. If we fail to listen and recover the importance of the Bible in personal and public life, then the sun may set on the West. This book is a tract for our times and a must-read for anyone concerned with impacting our culture." (Art Lindsley, author, C. S. Lewis's Case for Christ)
- "With solid, detailed information, clarity of presentation, and logical force, Vishal Mangalwadi enables anyone willing to see how our 'Western' world depends entirely upon what the Bible, and it alone, teaches about reality and how to live." (Dallas Willard, author, The Divine Conspiracy and The Great Omission)

## Vishal & Ruth Mangalwadi: The Legacy of William Carey

### A Model for the Transformation of a Culture [Amazon](https://www.amazon.com/Legacy-William-Carey-Transformation-Culture/dp/1581341121)

He was an industrialist.\
An economist.\
A medical humanitarian.\
A media pioneer.\
An educator.\
A moral reformer.\
A botanist.\
And a Christian missionary.\
And he did more for the transformation of the Indian subcontinent in the nineteenth and twentieth centuries than any other individual before or since.

Many know of William Carey. Some know about the specifics of his work and ministry. But few understand the profound contemporary significance of his life. Few realize how much we owe the increasing globalization of Christianity to the silent revolution he initiated. Fewer still are aware of his legacy of sensitivity to the variety of issues confronting true gospel witness in any culture.

This biography about the central character in the story of India's modernization and transformation will help you understand Carey's impact. But The Legacy of William Carey is more than a biography. It is a charge to all Christians to respond in kind within our own cultures, and to use Carey's example as our model for taking the light of the Gospel into every corner of society. If we follow in his footsteps, not only will lives be bettered this side of heaven, but hearts will be changed for eternity--and entire cultures transformed for Christ.

This book is part of a triology. The second book, Missionary Conspiracy: Letters to a Postmodern Hindu, goes beyond Carey to look at the whole of the nineteenth missionary movement, its relationship with British colonialism, and its impact to India. The third, India: The Grand Experiment, expands the scope of the study to include the the twentieth century, demonstrating that it was the Gospel, not Gandhi, that set India free.
