---
source: TrendForum
created: '2015-09-02'
author: Jürgen
title: Rotterdam's Mayor propose to turn the Pyramid
categories:
- Globalisering
- Maatschappij
---
Ahmed Aboutaleb, mayor of Rotterdam, wants to [strengthen the position of urban areas](https://www.nrc.nl/nieuws/2015/09/01/aboutaleb-meer-rijkstaken-naar-steden-is-een-must) with respect national government. Urban areas have to compete internationally independent from the nation state.
