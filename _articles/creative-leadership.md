---
source: TrendForum
created: '2012-10-21'
author: Willem
title: Creative leadership
categories:
- Overig
---
[John Maeda](https://en.wikipedia.org/wiki/John_Maeda) mentions in his [TED talk on art, technology, design and leadership](https://www.ted.com/talks/john_maeda_how_art_technology_and_design_inform_creative_leaders.html) traditional and creative leadership ([summarised here](https://developyourbballiq.com/art-and-leadership-in-coachng/)). I see links to [emotionally intelligent](https://www.digitalsignagetoday.com/video/1822/Daniel-Pink-describes-Emotionally-Intelligent-Signage) [signage](https://www.danpink.com/category/emotionally-intelligent-signage) and a [networked worldview](https://www.youtube.com/watch?v=nJmGrNdJ5Gw). Not new, but with an interesting link to art.
