---
source: TrendForum
created: '2014-06-15'
author: Jürgen
title: Coffee disintermediation
categories:
- Ontwikkelingssamenwerking
- Globalisering
- Techniek
---
Bonaverde: From green beans to fresh coffee in one [machine](https://bonaverde.pr.co/clippings/8724-bonaverde-from-green-beans-to-fresh-coffee-in-one-machine).
