---
source: TrendForum
created: '2014-06-24'
author: Frans
title: The vanished grandeur of accounting
categories:
- Geschiedenis
- Kerk
- Financiën
---
An [article](https://www.bostonglobe.com/ideas/2014/06/07/the-vanished-grandeur-accounting/3zcbRBoPDNIryWyNYNMvbO/story.html) by [Jacob Soll](https://en.wikipedia.org/wiki/Jacob_Soll).

"Double-entry accounting made it possible to calculate profit and capital and for managers, investors, and authorities to verify books. But at the time, it also had a moral implication. Keeping one’s books balanced wasn’t simply a matter of law, but an imitation of God, who kept moral accounts of humanity and tallied them in the Books of Life and Death. It was a financial technique whose power lay beyond the accountants, and beyond even the wealthy people who employed them."

See also his book [The Reckoning: Financial Accountability and the Rise and Fall of Nations](https://www.amazon.com/The-Reckoning-Financial-Accountability-Nations/dp/0465031528).

[Double-entry accounting](https://en.wikipedia.org/wiki/Double-entry_accounting) and the Father of Bookkeeping [Luca Pacioli](https://en.wikipedia.org/wiki/Luca_Pacioli)

[Jacob Soll on “The Reckoning: Financial Accountability and the Rise and Fall of Nations”](https://vimeo.com/93269098)
Good bookkeeping makes for good government—but not for very long—according to this history of accounting in the public sphere. In this talk, historian and MacArthur fellow Soll surveys public financial record keeping after the invention of double-entry accounting in 13th-century Tuscany, a breakthrough that made systematic analysis of profit and loss possible. The benefits for well-ordered, responsible government were felt wherever this innovation was embraced: medieval Italian city-states, 17th-century Holland, and 18th-century Britain all became economic and geopolitical powerhouses, he argues, thanks to well-kept government books. But this is even more a story of backsliding and failure, as corruption, spendthrift government, and factional intrigues perennially undermine the discipline of meticulous public accounting—and the state. Soll will highlight both the political impact of accounting practices—a public audit of Louis XVI’s woeful state finances helped spark the French Revolution—and the advent of a “culture of accountability” as the bourgeois business classes rose to power; and he will explore the deep-seated religious and philosophical power of balanced-books metaphors in the West, from the Gospel of Matthew to Thoreau’s championing of the simple life. He shows that numbers themselves do not equal financial progress and clarity.
There has been talk recently of the possibly demise of capitalism due to population curves. This presentation seeks to complicate this arguments by showing a history of capitalism based not on numerical constants, but rather on long cultural traditions, in particular double-entry bookkeeping and the construction of financially accountable societies.
