---
source: TrendForum
created: '2012-12-06'
author: Willem
title: Feminism and the need to treat all as moral agents
categories:
- Maatschappij
---
Perhaps an obvious [remark on Feminism](https://peterrollins.net/?p=4101) by Peter Rollins: just targetting men in feminism is an act of patriarchism. This fits well with his implicit/explicit (action/ideology) theme. A commentator writes, "A feminist critique is for all people, not just men. Feminism (as well as womanism) calls and informs women and men to critique the narratives they are living in and have received."
