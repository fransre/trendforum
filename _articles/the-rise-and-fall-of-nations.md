---
source: TrendForum
created: '2014-06-28'
author: Jürgen
title: '"... the Rise and Fall of Nations"'
categories:
- Geschiedenis
- Kerk
- Globalisering
- Maatschappij
- Financiën
---
Inspired by the subtitle: "... the Rise and Fall of Nations" of [The Reckoning: Financial Accountability and the Rise and Fall of Nations](https://trendforum.nl/the-vanished-grandeur-of-accounting/) by [Jacob Soll](https://en.wikipedia.org/wiki/Jacob_Soll)
here a list of books with topics of similar scope:

- [The Wealth and Poverty of Nations](https://en.wikipedia.org/wiki/The_Wealth_and_Poverty_of_Nations) by [David Landes](https://en.wikipedia.org/wiki/David_Landes)
- [Civilization: The West and the Rest](https://www.amazon.com/Civilization-West-Rest-Niall-Ferguson/dp/0143122061) by [Niall Ferguson](https://en.wikipedia.org/wiki/Niall_Ferguson)
- [Why Nations Fail: The Origins of Power, Prosperity, and Poverty](https://en.wikipedia.org/wiki/Why_Nations_Fail) by [Daron Acemoğlu](https://en.wikipedia.org/wiki/Daron_Acemoglu) and James Robinson
- [Guns, Germs, and Steel: The Fates of Human Societies](https://en.wikipedia.org/wiki/Guns,_Germs,_and_Steel) by [Jared Diamond](https://en.wikipedia.org/wiki/Jared_Diamond)
- and [Collapse: How Societies Choose to Fail or Succeed](https://en.wikipedia.org/wiki/Collapse:_How_Societies_Choose_to_Fail_or_Succeed)
- [The Rise and Fall of the Great Powers](https://en.wikipedia.org/wiki/The_Rise_and_Fall_of_the_Great_Powers) by [Paul Kennedy](https://en.wikipedia.org/wiki/Paul_Kennedy)
- [The Rise and Decline of Nations: Economic Growth, Stagflation, and Social Rigidities](https://www.amazon.com/Rise-Decline-Nations-Stagflation-Rigidities-ebook/dp/B00267SS7W) by [Mancur Olson](https://en.wikipedia.org/wiki/Mancur_Olson)
- [The J Curve: A New Way to Understand Why Nations Rise and Fall](https://en.wikipedia.org/wiki/The_J_Curve:_A_New_Way_to_Understand_Why_Nations_Rise_and_Fall) by [Ian Bremmer](https://en.wikipedia.org/wiki/Ian_Bremmer)
- [Vanished Kingdoms: The History of Half-Forgotten Europe](https://www.amazon.com/Vanished-Kingdoms-Rise-States-Nations/dp/0143122959) by [Norman Davies](https://en.wikipedia.org/wiki/Norman_Davies)
- [The Wealth of Nations](https://en.wikipedia.org/wiki/The_Wealth_of_Nations) by [Adam Smith](https://en.wikipedia.org/wiki/Adam_Smith)
- [A 'Short Treatise' on the Wealth and Poverty of Nations (1613)](https://www.amazon.com/Treatise-Wealth-Poverty-Nations-Economic/dp/085728973X) by [Antonio Serra](https://en.wikipedia.org/wiki/Antonio_Serra)

[The Most Influential Books for Eradicating Poverty](https://www.borgenmagazine.com/influential-books-eradicating-poverty/)

- [Political Order and Political Decay: From the Industrial Revolution to the Globalization of Democracy](https://www.amazon.com/Political-Order-Decay-Industrial-Globalization/dp/0374227357) by [Francis Fukuyama](https://en.wikipedia.org/wiki/Francis_Fukuyama)
- [World Order](https://www.amazon.com/World-Order-Henry-Kissinger/dp/1594206147) by [Henry Kissinger](https://en.wikipedia.org/wiki/Henry_Kissinger)
- and an [interview](https://www.spiegel.de/international/world/interview-with-henry-kissinger-on-state-of-global-politics-a-1002073.html) about world order

- [The Rise and Fall of Nations: Forces of Change in the Post-Crisis World](https://www.amazon.com/Rise-Fall-Nations-Forces-Post-Crisis/dp/0393248895) by [Ruchir Sharma](https://en.wikipedia.org/wiki/Ruchir_Sharma)
