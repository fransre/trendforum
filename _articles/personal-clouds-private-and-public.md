---
source: TechSquare
created: '2011-01-10'
author: Willem
title: 'Personal clouds: private and public'
categories:
- Cloud
---
While everyone is using public clouds in some form or another, data ownership is a problematic issue, and will be even more so.

![ownCload](https://owncloud.org/ownCloud/logo.png)

[ownCloud](https://owncloud.org/) is an effort to use of clouds on the open source desktop, but with the possibility for storing data on your own devices. There's a long way to go, but it's good to see this direction being taken up.
