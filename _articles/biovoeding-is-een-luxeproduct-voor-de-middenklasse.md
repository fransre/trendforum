---
source: TrendForum
created: '2014-12-08'
author: Willem
lang: nl
title: Biovoeding is een luxeproduct voor de middenklasse
categories:
- Wetenschap
- Maatschappij
---
In [Biovoeding is een luxeproduct voor de middenklasse](https://www.tijd.be/detail.art?a=9575885) vertelt [Louise Fresco](https://nl.wikipedia.org/wiki/Louise_Fresco) (sinds kort voorzitter van de raad van bestuur van de de Universiteit van Wageningen) gaat ze in op de relevantie van kunst voor wetenschap, de noodzaak voor wetenschap om de maatschappij aan te voelen, en maakt ze helder dat het om afwegingen gaat in de voedselwereld: klein biologisch gaat niet zomaar samen met iedereen voeden. "Valse romantiek".

Om de focus helder te houden.

Een [ander geluid vanuit Berkely](https://newscenter.berkeley.edu/2014/12/09/organic-conventional-farming-yield-gap/), waarin biologisch als volwaardig alternatief wordt gezien voor chemisch intensive teelt.

Lucas Simons: [Changing the Food Game](https://www.newforesight.com/news/newforesight-ceo-lucas-simons-releases-first-book/)

Op het [LLTB](https://www.lltb.nl/home/) Jaarcongres 2014 (november) [vertelt](https://www.youtube.com/watch?v=RaBgXXbsspI) Aalt Dijkhuizen dat productiviteit goed is voor het milieu en biologisch niet zozeer "duurzaam" op alle vlakken, dat de wereldvoedselvoorziening nog steeds onze aandacht verdient, en dat iedereen moet doen waar 'ie het beste in is en ook voor duurzaamheid grondstoffen het beste getransporteerd kunnen worden.

Mooie oproep tot naar de cijfers kijken.

Voedselcollectief Eindhoven [stopt](https://www.ed.nl/regio/eindhoven/voedselcollectief-eindhoven-stopt-1.4837869)
