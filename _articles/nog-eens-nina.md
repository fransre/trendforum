---
source: TrendForum
created: '2010-10-13'
author: Jürgen
lang: nl
title: Nog eens Nina
categories:
- Kerk
---
[In het Nederlands](https://www.youtube.com/watch?v=JShNOCNb58c)

Zij heeft ook een Nederlands verleden, naast [Personal Jesus](https://www.personaljesus.de/).

Leuk om te zien! Ze zegt dat ze God altijd al gezocht heeft. Goed dat ze 'em uiteindelijk heeft gevonden. En het Nederlandse verleden is niet zo positief met Herman Brood en veel cocaïne...
