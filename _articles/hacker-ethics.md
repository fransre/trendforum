---
source: TechSquare
created: '2011-08-13'
author: Jürgen
title: Hacker ethics
categories:
- Privacy
---
CCC published a [list of principles](https://dasalte.ccc.de/hackerethics) which is an extension of one by Steven Levy in his book Hackers: Heroes of the Computer Revolution:

* Access to computers - and anything which might teach you something about the way the world really works - should be unlimited and total. Always yield to the Hands-On Imperative!
* All information should be free.
* Mistrust authority - promote decentralization.
* Hackers should be judged by their acting, not bogus criteria such as degrees, age, race, or position.
* You can create art and beauty on a computer.
* Computers can change your life for the better.
* Don't litter other people's data.
* Make public data available, protect private data.

Many of the recent actions do not conform to these principles.
