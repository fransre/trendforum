---
source: TrendForum
created: '2012-04-29'
author: Jürgen
title: 'Robert Shiller: Finance and The Good Society'
categories:
- Maatschappij
- Financiën
---
[Robert Shiller](https://en.wikipedia.org/wiki/Robert_Shiller) looks in his new book: [Finance and The Good Society](https://www.amazon.com/Finance-Good-Society-Robert-Shiller/dp/0691154880) at ways financial engineering can benefit society.

[Here](https://www.youtube.com/watch?v=ZeC34GWALD0) he gives an introductory interview.


Also interesting is the last class: [23. Finding your Purpose in a World of Financial Capitalism](https://www.youtube.com/watch?v=2_pDTWgJg94
) in his course: [Financial Markets (2011)](https://www.youtube.com/playlist?list=PL8FB14A2200B87185) in which he looks at the broader society and the role financial technology can play in it:

"After reviewing the main themes of this course, Professor Shiller shares his views about finance from a broader perspective. His first topic, the morality of finance, centers on Peter Unger's Living High and Letting Die and William Graham Sumner's What the Social Classes Owe Each Other. Subsequently, he addresses the hopelessness about the world's future that some see from Malthus' dismal law from the Essay on the Principle of Population, but contrasts it with a positive outlook on purposes and goals in life. While discussing the endurance and survival of financial contracts, he outlines the cases of Germany after World War I, Iran after the Islamic Revolution, and South Africa after the end of apartheid, in which financial contracts prevailed, but does not fail to mention the cases of Russia after the Russian Revolution and Japan after World War II, in which it has not been the case. After a brief comparison between Mathematical Finance and Behavioral Finance, he elaborates on the interplay between wealth and inequality, building on Jacob Hacker's and Paul Pearson's Winner-Take-All Politics, Karl Marx's Das Kapital, and Robert K. Merton concept of the cosmopolitan class. Following this, he emphasizes the democratization of finance as an important future trend and provides examples for this process from his books The Subprime Solution,The New Financial Order and Finance and the Good Society. Professor Shiller concludes the course with advice for finding the right career, highlighting the role of random events, but also the importance of a long-horizon outlook and an orientation towards history in the making."

[Course material: books](https://yalepress.yale.edu/yupbooks/onlinecatalog.asp?catalog=6522043&discount=Y&allsections=Y)
Some of the books he makes specific reference to:

Fabozzi: [Foundations of Financial Markets and Institutions](https://www.amazon.com/Foundations-Financial-Markets-Institutions-Edition/dp/0136135315/)
Hacker / Pierson: [Winner-Take-All Politics: How Washington Made the Rich Richer--and Turned Its Back on the Middle Class](https://www.amazon.com/Winner-Take-All-Politics-Washington-Richer-Turned/dp/1416588701)
Robert K. Merton: [On Social Structure and Science](https://www.amazon.com/Social-Structure-Science-Heritage-Sociology/dp/02265207140)

Press Conference with Robert Shiller about [winning](https://www.youtube.com/watch?v=IK-idXz0dhw) the Nobel Prize in Economics.
The presentation is a lot about describing the areas of work, achievements and larger societal intentions.

In his press conference Robert Shiller names [James Tobin](https://en.wikipedia.org/wiki/James_Tobin) his mentor and the reason for going to Yale.
