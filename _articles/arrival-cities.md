---
source: TrendForum
created: '2013-04-13'
author: Jürgen
title: Arrival cities
categories:
- Globalisering
- Maatschappij
---
Doug Saunders identifies [arrival cities](https://arrivalcity.net/about/) as the main concept for urbanization:
"Rural life is the single biggest killer of humanity in modern times. Slums are huge improvement over rural life. Slums are places of hope"

[3 videos](https://arrivalcity.net/video/)

1. An example of a Chinese arrival city
2. Interview with Saunders: he compares the development in Turkey and Brazil with inside migration and the Bangladeshian community in East London and other Western cities. Slums improve over time. Very successful slums seem to get poorer. However this is due to the high rate of moving out in better areas in the city and moving in from the rural areas. Governments which restrict small businesses in normal houses block development of the immigrants, which is mostly the case in Europe. He projects a shortage of unskilled labour for most Western countries in the next decade, even if this is politically difficult to acknowledge.
3. IKON documentary on arrival cities with Doug Saunders. Similar to the 2nd video. Amsterdam is shown as a negative example because of its rigid urban policies.

Arrival cities are a multi-generational concept.

Wow, something I don't often realise. Thanks for sharing!
