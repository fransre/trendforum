---
source: TrendForum
created: '2014-06-29'
author: Jürgen
title: 'Gates: ''Simple Things Can Have a Huge Effect'''
categories:
- Ontwikkelingssamenwerking
- Globalisering
- Maatschappij
- Financiën
---
The Bill and Melinda Gates Foundation is the largest private global development organization in the world. In a SPIEGEL interview, Melinda Gates [explains](https://www.spiegel.de/international/world/interview-with-melinda-gates-on-development-aid-and-charity-a-976931-druck.html) the couple's start in philanthropy, the challenges of combatting disease in conflict zones and the unique responsibility of the wealthy.


