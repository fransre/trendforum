---
source: TrendForum
created: '2014-09-20'
author: Jürgen
title: What is important for citizenship in the digital society
categories:
- Globalisering
- Maatschappij
---
Political principles

- freedom and responsibility

Social principles

- each subject should be free to choose a fitting level of transparency
- data ownership: data is owned by its subject
- good intentions do not necessarily lead to good results

Architectural principles

- multiple partial identities
- logical distribution
- potential for distributed implementation
