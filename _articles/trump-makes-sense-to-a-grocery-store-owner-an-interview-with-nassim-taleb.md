---
source: TrendForum
created: '2017-02-12'
author: Frans
title: "‘Trump makes sense to a grocery store owner’ - an interview with Nassim Taleb"
categories:
- Overig
---
[‘Trump makes sense to a grocery store owner’](https://www.thehindu.com/books/‘Trump-makes-sense-to-a-grocery-store-owner’/article17109351.ece)

* The real problem is the ‘faux-expert problem’, one who doesn’t know what he doesn’t know, and assumes he knows what people think.
* I think in the long term, the world can only survive if it lives like nature does. Many smaller units of governance, and a collection of super islands with some separation, quick decision-making, and visible implementation. Lots of Switzerlands, that’s what we need.
* What we need is not leaders, we don’t need them. We just need someone at the top who doesn’t mess the system up.
