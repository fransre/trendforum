---
source: TrendForum
created: '2016-02-19'
author: Jürgen
title: Watson Health
categories:
- Wetenschap
- Techniek
- Maatschappij
---
IBM [Buys Truven](https://www.nytimes.com/2016/02/19/technology/ibm-buys-truven-adding-to-growing-trove-of-patient-data-at-watson-health.html), Adding to Growing Trove of Patient Data at Watson Health

The Watson Health business, IBM said, now has health-related data on “approximately 300 million patient lives,” mostly in the United States. The goal is to run the patient data through Watson’s artificial intelligence software, so that it works as a specialized digital assistant to physicians and health administrators to improve care and curb costs.

see [section Health](https://en.wikipedia.org/wiki/Watson_%28computer%29)
