---
source: TrendForum
created: '2012-01-12'
author: Willem
title: We need to talk
categories:
- Overig
---
Diplomat [Jonas Gahr Støre](https://en.wikipedia.org/wiki/Jonas_Gahr_St%C3%B8re) [explains](https://www.ted.com/talks/jonas_gahr_store_in_defense_of_dialogue.html) that traditional diplomacy focuses on states, while 'groups' are becoming more important. And "that very few of these domestic inter-/intra-state conflicts can be solved militarily. They may have to be dealt with by military means, but they cannot be solved by military means."

[Jonas Gahr Støre: In defense of dialogue](https://www.youtube.com/watch?v=3lxyi_3CGrk)

"Our modern western societies are more complex than before, in this time of migration. How are we going to settle, and build a bigger 'we' to deal with our issues, if we don't improve our skills of communication?" And this applies to diplomacy as well as 'just' what happens in our societies when there is no conflict.

"We must build a bigger we"

Great stuff, very basic, very essential!

" .... sometimes we must fight ...." Do you agree with him?
