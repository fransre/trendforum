---
source: TechSquare
created: '2014-03-08'
author: Jürgen
title: Industry 4.0
categories:
- Policy
---
[Industry 4.0](https://en.wikipedia.org/wiki/Industry_4.0) is a project in the high-tech strategy of the German government, which promotes the computerization of traditional industries such as manufacturing.
In [German](https://de.wikipedia.org/wiki/Industrie_4.0):
Die Bezeichnung „Industrie 4.0“ soll die vierte industrielle Revolution zum Ausdruck bringen. Die erste industrielle Revolution bestand in der Mechanisierung mit Wasser- und Dampfkraft, darauf folgte die zweite industrielle Revolution: Massenfertigung mit Hilfe von Fließbändern und elektrischer Energie, daran anschließend die Digitale Revolution, der Einsatz von Elektronik und IT zur weiteren Automatisierung der Produktion wurde üblich.
[Pleidooi](https://www.mechatronicamachinebouw.nl/artikel/nederland-moet-nu-inhaken-op-industrie-40.html) voor een Nederlandse reactie.

De Nederlandse reactie: [Smart Industry](https://www.fme.nl/content.jsp?objectid=55655)

 \+ [commentaar](https://www.fme.nl/Actueel/Content/Weblog_voorzitter/Oranje_Messe)

[TUE](https://www.mechatronicamachinebouw.nl/artikel/tue-mechatronica-20-industrie-40.html) = Mechatronica 2.0 + Industrie 4.0

Nieuw boek: [Smart Industry](https://smartindustry.info/) - De nieuwe groeimotor en banenmachine van Nederland

Deze slimme industrie gaat zorgen voor honderdduizenden nieuwe banen in het mkb.
