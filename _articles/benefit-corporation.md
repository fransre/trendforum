---
source: TrendForum
created: '2012-04-29'
author: Jürgen
title: Benefit corporation
categories:
- Maatschappij
- Financiën
---
A [benefit corporation](https://en.wikipedia.org/wiki/Benefit_corporation) is a class of corporation required by law to create general benefit for society as well as for shareholders. Benefit corporations must create a material positive impact on society, and consider how their decisions affect their employees, community, and the environment. Moreover, they must publicly report on their social and environmental performances using established third-party standards.
