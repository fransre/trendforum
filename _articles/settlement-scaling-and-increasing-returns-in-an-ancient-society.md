---
source: TrendForum
created: '2015-02-22'
author: Jürgen
title: Settlement scaling and increasing returns in an ancient society
categories:
- Geschiedenis
- Globalisering
- Wetenschap
- Maatschappij
---
A key property of modern cities is increasing returns to scale—the finding that many socioeconomic outputs increase more rapidly than their population size. Recent theoretical work proposes that this phenomenon is the [result of general network effects](https://advances.sciencemag.org/content/1/1/e1400066) typical of human social networks embedded in space and, thus, is not necessarily limited to modern settlements.

[Dutch](https://www.nu.nl/wetenschap/3997532/moderne-steden-hebben-hetzelfde-groeipatroon-als-antieke-stad.html): Moderne steden hebben hetzelfde groeipatroon als antieke stad
