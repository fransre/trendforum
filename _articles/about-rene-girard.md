---
source: TrendForum
created: '2011-09-24'
author: Jürgen
title: About René Girard
categories:
- Geschiedenis
- Kerk
- Globalisering
- Maatschappij
---
His [ideas](https://en.wikipedia.org/wiki/Ren%C3%A9_Girard):

* mimetic desire: imitation is an aspect of behaviour that not only affects learning but also desire, and imitated desire is a cause of conflict,
* the scapegoat mechanism is the origin of sacrifice and the foundation of human culture, and religion was necessary in human evolution to control the violence that can come from mimetic rivalry,
* the Bible reveals the two previous ideas and denounces the scapegoat mechanism.

More links:

* [Video](https://www.youtube.com/watch?v=BNkSBy5wWDk) of René Girard introducing his ideas.
* Peter Thiel, co-founder and ex-ceo of PayPal and angel investor of Facebook, [on René Girard](https://www.youtube.com/watch?v=esk7W9Jowtc)
* Nederlandse [introductie](https://www.bezinnen.nl/girard/index.php?option=com_content&view=article&id=123:kerngedachten&catid=34:ess&Itemid=133)
* Introduction to the [ideas of René Girard](https://www.cottet.org/girard/index.en.htm)
* Dutch Girard Society: [Girard Studiekring](https://www.bezinnen.nl/girard/)
* [Wikipedia](https://en.wikipedia.org/wiki/Ren%C3%A9_Girard)
* [Stanford Alumni magazin](https://www.stanfordalumni.org/news/magazine/2009/julaug/features/girard.html)

Text introducing his latest book: Sacrifice:

* In Sacrifice, René Girard interrogates the Brahmanas of Vedic India, exploring coincidences with mimetic theory that are too numerous and striking to be accidental. Even that which appears to be dissimilar fails to contradict mimetic theory, but instead corresponds to the minimum of illusion without which sacrifice becomes impossible. The Bible reveals collective violence, similar to that which generates sacrifice everywhere, but instead of making victims guilty, the Bible and the Gospels reveal the persecutors of a single victim. Instead of elaborating myths, they tell the truth absolutely contrary to the archaic sense. Once exposed, the single victim mechanism can no longer function as the model for would-be sacrificers. Recognizing that the Vedic tradition also converges on a revelation that discredits sacrifice, mimetic theory locates within sacrifice itself a paradoxical power of quiet reflection that leads, in the long run, to the eclipse of this institution which is violent but nevertheless fundamental to the development of human culture. Far from unduly privileging the Western tradition and awarding it a monopoly on the knowledge and repudiation of blood sacrifice, mimetic analysis recognizes comparable, but never truly identical, traits in the Vedic tradition.
(taken from [Amazon](https://www.amazon.com/Sacrifice-Breakthroughs-Mimetic-Theory-Girard/dp/0870139924) )

[Imitatio](https://www.imitatio.org) was conceived as a force to press forward the consequences of René Girard’s remarkable insights into human behavior and culture.

It is [sponsored](https://www.imitatio.org/about-imitatio/about-imitatio.html) by [Peter Thiel](https://en.wikipedia.org/wiki/Peter_Thiel) a former student of Girard.

A [disturbing lecture](https://www.girardlectures.org/) given by [Timothy Snyder](https://en.wikipedia.org/wiki/Timothy_Snyder) in the honor of Rene Girard: "Why Did the Holocaust Happen? A History Lesson for the Future". He describes the internal logic leading to the Holocaust, mostly the possibility when state structure is gone.

Timothy Snyder wrote [Bloodlands: Europe Between Hitler and Stalin](https://www.bloodlandsbook.com/),
and an essay on the same topic: [Holocaust: The ignored reality](https://www.eurozine.com/articles/2009-06-25-snyder-en.html)
