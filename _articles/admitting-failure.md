---
source: TrendForum
created: '2011-12-29'
author: Willem
title: Admitting failure
categories:
- Ontwikkelingssamenwerking
- Maatschappij
---
with a focus on NGOs. [Engineers without borders](https://www.ewb-international.org/) started publishing yearly [failure reports](https://legacy.ewb.ca/en/whoweare/accountable/failure.html).

[Learning from failure | David Damberger | TEDxYYC](https://www.youtube.com/watch?v=HGiHU-agsGY)
