---
source: TrendForum
created: '2014-06-20'
author: Jürgen
title: Boarding schools
categories:
- Maatschappij
---
- The British boarding school [remains](https://www.theguardian.com/commentisfree/2012/jan/16/boarding-school-bastion-cruelty) a bastion of cruelty
- Why boarding schools [produce](https://www.theguardian.com/education/2014/jun/09/boarding-schools-bad-leaders-politicians-bullies-bumblers) bad leaders.

