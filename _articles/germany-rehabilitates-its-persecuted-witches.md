---
source: TrendForum
created: '2011-12-17'
author: Jürgen
title: Germany Rehabilitates Its Persecuted 'Witches'
categories:
- Geschiedenis
- Maatschappij
---
[Story](https://www.spiegel.de/international/germany/0,1518,804288,00.html)

I think also here applies [the point of Bernhard Schlink](https://trendforum.nl/bernhard-schlink-on-forgiveness-and-reconciliation/) that as someone from a later generation is not guilty but has to take position.
