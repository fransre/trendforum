---
source: Enkairoi
created: '2009-05-30'
author: Jürgen
title: Digital Identity
categories:
---

about Digital Identity
- [Future of Identity in the Information Society (FIDIS)](http://www.fidis.net/home/)
- [Identity Commons Wiki](http://wiki.idcommons.net/Main_Page)
- [Wikipedia: Digital Identity](https://en.wikipedia.org/wiki/Digital_identity)
- [Loosely Coupled](http://www.looselycoupled.com/glossary/digital%20identity)
- [Kim Cameron's Identity Blog](https://www.identityblog.com/)
- [ZDnet's DigitalID Blog](https://www.zdnet.com/article/welcome-to-the-digital-id-world-blog/)
- [Digital Identity by Phillip Windley](https://www.oreilly.com/library/view/digital-identity/0596008783/)
- [The Story of Digital Identity (podcasts)](https://open.spotify.com/show/1EBEiBC3dtb1txg2x9Dh4H)
- [Digital Money Forum](https://digitaldebateblogs.typepad.com/digital_money/)
