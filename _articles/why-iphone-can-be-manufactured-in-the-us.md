---
source: TrendForum
created: '2012-01-23'
author: Jürgen
title: Why iPhones can not be manufactured in the US
categories:
- Globalisering
- Techniek
---
[Interesting story](https://www.nytimes.com/2012/01/22/business/apple-america-and-a-squeezed-middle-class.html?pagewanted=all) about the logic for shifting manufacturing to China.

More about Apple's [manufacturing](https://www.nytimes.com/2012/01/26/business/ieconomy-apples-ipad-and-the-human-costs-for-workers-in-china.html?_r=2&pagewanted=all).

Tim Cook responds in an [internal email](https://9to5mac.com/2012/01/26/tim-cook-responds-to-claims-of-factory-worker-mistreatment-we-care-about-every-worker-in-our-supply-chain/)
