---
source: TechSquare
created: '2011-10-22'
author: Jürgen
title: Large-scale off-shore wind farms
categories:
- Energy
---
[Story](https://www.spiegel.de/international/germany/0,1518,792918,00.html): The term "energy revolution" sounds light and airy enough, but how do human beings manage to wrest electricity from the sea? Germany's largest offshore wind farm, a power plant surrounded by a hostile environment, produces 12 times as much energy as the world's first nuclear power plant.
