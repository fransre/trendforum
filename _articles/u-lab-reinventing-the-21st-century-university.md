---
source: TrendForum
created: '2015-01-04'
author: Willem
title: 'U.Lab: Reinventing the 21st Century University'
categories:
- Overig
- Globalisering
- Wetenschap
---
Dat ons onderwijssysteem misschien niet geheel meer past in onze tijd, is een boodschap die al [langer rondwaart](https://www.youtube.com/watch?v=zDZFcDGpL4U). Nu komt MIT met een nieuw experiment, om het hart een plek te geven. Niet alleen verstand en handen.

[edX course](https://www.edx.org/course/u-lab-transforming-business-society-self-mitx-15-s23x) | [Video intro](https://www.youtube.com/watch?v=uxu0AvAALiM)

The program uses an interesting list of worthwhile things:

Each week, you will:

- Engage in self-reflection (personal)
- Go out into the world and apply a specific tool (practical)
- Meet in a coaching circle with four fellow u.lab participants (relational)
- Be introduced to a mindfulness practice (mindful)
- Join three live streamed classes in which all participants come together to learn in real time (collective)
- Engage in transformative practices that allow you to connect to your highest future potential (transformative)

[Eindhoven krijgt](https://www.bits-chips.nl/artikel/eindhoven-krijgt-singularity-university.html) Singularity University
