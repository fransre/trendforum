---
source: Enkairoi
created: '2009-11-25'
author: Willem
title: Edwin Judge
categories:
- People
---

Emeritus professor Edwin Judge has contributed to developments in the understanding of ancient history and early christianity. One of his major points is "the social scientific criticism of the new testament".

### On christianity and science

In a video he argues that while christianity is often cast as the enemy of science, it has in fact made it possible.

Around 600 BC, the Greek philosophers
- assume that the kosmos consists of material objects (incl. gods and all) of which there were four: earth, fire, air, water
   - [discussion: what about Plato's Idea? that's no material and seen as more essential than matter]
- take for granted: everything is logical, necessary, and static. What appear to be changes are in effect, only, cycles. Everything is in motion, but not changing.
   - [discussion: does Heraclitus' "panta rei" challenge this or not?]

the christian understanding of the world
- God in Hebrew bible and new testament stood absolutely outside of the created, not part of the material substance.
- The kosmos had a beginning, and presumably also and end. So there is change, not just cyclical motion.

The modern scientific method, which is empirical testing, is a reaction against Greek science. It began from the doctrine of creation: The world is a rational whole, not eternally the same, but an objective phenomenon with a beginning, and changing.

Quoting him: "The whole business of testing for truth was explicitly rejected in classical culture as being illogical, irrational. The impact of seeing the physical universe as an objective thing that could be analyzed and explained, for what it actually was, and not what it in principle ought to have been, was very slow in coming. But the slow progress nevertheless represents the takeover of our worldview of the biblical perspective. (..) In spite of the fact that people keep looking back to it as the origin of science. It's not the origin of science, the book of creation is the origin of science, the book of Genesis."

### See also
- Stanley Jaki
- Openheimer & Whitehead, altough not christians themselves, agree that science is only possibly because of the christian worldview.
- Adolf Schlatter makes it clear in his talks as well
