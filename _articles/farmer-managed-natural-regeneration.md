---
source: TrendForum
created: '2012-06-23'
author: Jürgen
title: Farmer Managed Natural Regeneration
categories:
- Ontwikkelingssamenwerking
---
[FMNR](https://en.wikipedia.org/wiki/Farmer_Managed_Natural_Regeneration) is reforestation technique created by Tony Rinaudo. [He tells the story](https://www.youtube.com/watch?v=E9DpptI4QGY), [NYTimes](https://www.nytimes.com/2007/02/11/world/africa/11niger.html?_r=1&pagewanted=all) as well.
An [article](https://permaculture.org.au/2008/09/24/the-development-of-farmer-managed-natural-regeneration/) by Tony about FMNR.

[Redefining agricultural yields: from tonnes to people nourished per hectare](https://iopscience.iop.org/1748-9326/8/3/034015)
part of the abstract: "...We find that, given the current mix of crop uses, growing food exclusively for direct human consumption could, in principle, increase available food calories by as much as 70%, which could feed an additional 4 billion people (more than the projected 2–3 billion people arriving through population growth). ..."
