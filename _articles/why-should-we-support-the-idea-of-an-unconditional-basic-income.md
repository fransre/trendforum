---
source: TrendForum
created: '2014-06-04'
author: Frans
title: Why Should We Support the Idea of an Unconditional Basic Income?
categories:
- Maatschappij
- Financiën
---
[An answer to a growing question of the 21st century](https://medium.com/tech-and-inequality/why-should-we-support-the-idea-of-an-unconditional-basic-income-8a2680c73dd3)

American: [Basic Income Guarantee](https://en.wikipedia.org/wiki/Basic_income_guarantee)
Dutch: [Basisinkomen](https://nl.wikipedia.org/wiki/Basisinkomen)
"The issue of the basic income gained prominence on the political agenda in Netherlands between the mid-1970s and mid-1990s but it has disappeared from the political agenda over the last fifteen years."
German: [Bedingungsloses Grundeinkommen](https://de.wikipedia.org/wiki/Bedingungsloses_Grundeinkommen)
  [Götz Werner](https://en.wikipedia.org/wiki/G%C3%B6tz_Werner)
The German wikipedia also has presentation of the history if BIG, starting with Thomas Morus in Utopia (1516) and Juan Luis Vives (1492–1540).
Namibian: [Basic Income Grant](https://en.wikipedia.org/wiki/Omitara)
WW: [Basic Income Earth Network](https://en.wikipedia.org/wiki/Basic_Income_Earth_Network)

Nic Douben: [Heilig geloof in falende markt](https://www.ed.nl/mening/heilig-geloof-in-falende-markt-1.3976646)

The Dutch historian and journalist [Rutger Bregman](https://nl.wikipedia.org/wiki/Rutger_Bregman) wants to promote utopian ideas:
[Utopian Ideas for Realists](https://www.amazon.de/Utopia-Realists-how-can-there/dp/1408890275)
[Why we should give everyone a basic income](https://www.youtube.com/watch?v=aIL_Y9g7Tg0)
