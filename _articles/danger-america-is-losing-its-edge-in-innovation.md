---
source: TechSquare
created: '2011-01-24'
author: Frans
title: 'Danger: America Is Losing Its Edge In Innovation'
categories:
- Education
---
In a global, knowledge-driven economy there is a direct correlation between engineering education and innovation.

Global leadership is not a birthright. Despite what many Americans believe, our nation does not possess an innate knack for greatness. Greatness must be worked for and won by each new generation.

[Danger: America Is Losing Its Edge In Innovation](https://blogs.forbes.com/ciocentral/2011/01/20/danger-america-is-losing-its-edge-in-innovation/)

Good article, but
"Innovation is the key to survival in an increasingly global economy." sounds a bit heavy. He probably meant leadership instead of survival.
