---
source: TrendForum
created: '2014-05-20'
author: Jürgen
title: 'Slavery: a profitable labor model'
categories: []
---
ILO says forced labour [generates annual profits](https://www.ilo.org/global/about-the-ilo/newsroom/news/WCMS_243201/lang--en/index.htm) of US$ 150 billion.
Report finds illegal gain from forced labour of about 21 million people amounts to three times more than prior estimates.
