---
source: TrendForum
created: '2016-07-02'
author: Jürgen
title: The Netherlands, The Greenest City In The World
categories:
- Globalisering
- Techniek
- Maatschappij
---
The Netherlands itself is [like a city in a river delta](https://sustainableurbandelta.com/): with a single area of conurbation from north of Alkmaar to south of Eindhoven. With that image in mind, the ‘Green Heart’ of the Netherlands is just a ‘park’ and the Westland region is ‘urban farming’. Today, the Netherlands is the greenest city in the world.

[Priva](https://www.priva.co.uk/) is evolving rapidly: we are preparing for the world of tomorrow. With our solutions for climate and process control for the horticulture industry and the built environment, we create a climate for growth; solutions that enable people and plants to function optimally.


[Meiny Prins](https://www.priva.nl/nl/over-priva/meiny-prins/)
