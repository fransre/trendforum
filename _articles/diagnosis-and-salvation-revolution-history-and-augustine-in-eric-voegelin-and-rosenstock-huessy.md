---
source: TrendForum
created: '2011-05-11'
author: Jürgen
title: 'Diagnosis and Salvation: Revolution, History and Augustine in Eric Voegelin
  and Rosenstock-Huessy'
categories:
- Geschiedenis
- Globalisering
- Maatschappij
---
Wayne Cristaudo compares Eric Voegelin and Eugen Rosenstock-Huessy in a [paper](https://www.lsu.edu/artsci/groups/voegelin/society/2010%20Papers/Wayne%20Cristaudo.pdf) presented at the Eric Voegelin Society Meeting 2010.

Both are critical of modern society and both value Christianity and history. However, as Cristaudo makes clear, Rosenstock-Huessy sees Christianity and history not purely as an instrument of diagnosis, as does Voegelin. For Rosenstock-Huessy, history is the way in which the Christian Heilsgeschichte comes to the world. He, therefore, can also identify positive outcomes from the catastrophes of the 20th century and the revolutions of the Enlightenment period.

Cristaudo begins his article with his own journey from Voegelin to Rosenstock-Huessy, away from Platon and Platonist reading of Christianity to an embrace of a way of thinking that is one where love and evil as energies rather than reason predominate, if one will, a more Judaic-Christian hybrid.
