---
source: TechSquare
created: '2014-12-21'
author: Jürgen
title: Protonet
categories:
- Privacy
- Security
- Devices
---
[Protonet](https://protonet.info/en/) small German start-up ([intro](https://www.youtube.com/watch?v=QocHpp3fGTU) video):
Providing small and medium enterprises with a unique one-button social infrastructure and enabling them to control their own secure private cloud - hassle free!

- personal (cloud) server
- social communication ([preview](https://www.youtube.com/watch?v=GVFCf0bddIo))
- SOUL OS: Ubuntu-based, virus-protection ...?
-- extensible for all Windows/Linux/Unix based server SW via a VM

- the most technical [info](https://protonet.info/de/produkt/faq/) in German
- current standard access via proxy: "servername.protonet.info" (reverse-proxy-[problem](https://t3n.de/news/protonet-sicherheitsrisiko-517435/))
- Youtube [channel](https://www.youtube.com/channel/UCF0X3yp0Q708L07Cd3d7Ixw)
- [Github](https://github.com/protonet)
