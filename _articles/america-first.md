---
source: TrendForum
created: '2022-02-27'
author: Jürgen
title: America First
categories:
- Geschiedenis
- Maatschappij
- Globalisering
---
[America First](https://en.wikipedia.org/wiki/America_First_(policy)) refers to a policy stance in the United States coined by progressive, internationalist president [Woodrow Wilson](https://en.wikipedia.org/wiki/Woodrow_Wilson) that generally emphasizes nationalism and non-interventionism. The isolationist approach gained prominence in the interwar period (1918–1939) and was advocated by the [America First Committee](https://en.wikipedia.org/wiki/America_First_Committee), a non-interventionist pressure group against the U.S. entry into World War II.

In Donald Trump's 2016 presidential campaigns and presidency (2017–2021), Trump used the phrase as a slogan, emphasizing the United States' withdrawal from international treaties and organizations. "America First" was the official foreign policy doctrine of the Trump administration.

"America First" was a phrase used by the [Ku Klux Klan](https://en.wikipedia.org/wiki/Ku_Klux_Klan) at its peak in the 1920s, where racist, xenophobic sentiment was widespread. The Immigration Act of 1924 sponsored by Washington U.S. representative Albert Johnson proved to legislate xenophobia and white supremacy, excluding immigrants on the basis of ethnicity and national origin in an effort to preserve white racial demographics.

A list of meanings of [America First](https://en.wikipedia.org/wiki/America_First).
