---
source: TrendForum
created: '2016-05-08'
author: Jürgen
title: The Secret Shame of Middle-Class Americans
categories:
- Maatschappij
- Financiën
---
[The Secret Shame of Middle-Class Americans](https://www.theatlantic.com/magazine/archive/2016/05/my-secret-shame/476415/)
Nearly half of Americans would have trouble finding $400 to pay for an emergency. I’m one of them.
