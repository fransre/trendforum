---
source: TrendForum
created: '2011-01-29'
author: Jürgen
title: 'Os Guinness: The Case for Civility'
categories:
- Globalisering
- Maatschappij
---
An [interview](https://www.discerningreader.com/blog/2008/02/author-interview-os-guinness) with Os Guinness, the author of [The Case for Civility
And Why Our Future Depends on It](https://www.discerningreader.com/book-reviews/the-case-for-civility)

citing from a review:
"Much of the answer to whether or not we’ll learn to live with our deepest differences depends on rejecting two erroneous responses to the culture wars. First, we must say no to a “sacred public square”—a situation where one religion has a position of privilege or prominence that is denied to others. As he refutes the sacred public square, Guinness laments the state of the Religious Right and the damage it has done to faith in America. We must also say no to a “naked public square”—the situation where public life is left devoid of any religion. This is what is advocated by the new atheists. Both of these responses to the culture war are in contradiction to the Constitution.

The alternative to both is a “civil public square.” “The vision of a civil public square is one in which everyone—peoples of all faiths, whether religious or naturalistic—are equally free to enter and engage public life on the basis of their faiths, as a matter of ‘free exercise’ and as dictated by their own reason and conscience; but always within the double framework, first of the Constitution, and second, of a freely and mutually agreed covenant, or common vision for the common good, of what each person understands to be just and free for everyone else, and therefore of the duties involved in living with the deep differences of others.” If we are to have a civil society, we must first have a civil public square."

An [interview](https://www.publicchristianity.com/Videos/osguinness.html) (two videos) with Os Guinness about The Case for Civility.
He certainly does not know Rosenstock-Huessy as he assumes an endless order of civilizations.
