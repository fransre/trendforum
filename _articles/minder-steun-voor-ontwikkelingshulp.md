---
source: TrendForum
created: '2010-10-19'
author: Jürgen
lang: nl
title: Minder steun voor ontwikkelingshulp
categories:
- Ontwikkelingssamenwerking
---
[Minder steun voor ontwikkelingshulp](https://www.nrc.nl/binnenland/article2633184.ece/Minder_steun_voor_ontwikkelingshulp)

Blijkbaar begrijpen veel mensen de noodzaak niet.

Zou het beter zijn mensen uit de betreffende landen te laten spreken voor hun situatie dan onze gesubsidieerde professionals?
