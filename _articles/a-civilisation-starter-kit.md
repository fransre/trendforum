---
source: TrendForum
created: '2011-08-09'
author: Willem
title: A civilisation starter kit
categories:
- Techniek
---
[Marcin Jakubowski](https://opensourceecology.org/wiki/Marcin_Jakubowski) presents [open sourced blueprints for civilisation](https://www.ted.com/talks/marcin_jakubowski.html): DIY technology for "starting a civilisation".

50 machines in open hardware technology (plans, construction guidelines, instruction videos) that are needed for living a modern life.

The word "civilization" seems much too heavy. Copyleft hardware or machines which are useful and enduring independently from commercial suppliers is a much better description.
