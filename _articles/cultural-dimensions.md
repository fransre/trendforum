---
source: TrendForum
created: '2014-01-15'
author: Jürgen
title: Cultural dimensions
categories:
- Globalisering
- Maatschappij
- Management
---
The [Hofstede Centre](https://geert-hofstede.com/index.php) has extended the original 4 [national dimensions](https://geert-hofstede.com/national-culture.html) to now 6. The name of the 5th changed from confucian dynamism to long term orientation:

- Power Distance (PDI)
- Individualism versus collectivism (IDV)
- Masculinity versus femininity (MAS)
- Uncertainty avoidance (UAI)
- Long-term versus short-term orientation (LTO)
- Indulgence versus Restraint (IND)

Furtermore are there now organisational culture dimensions:

- Means oriented vs Goal oriented
- Internally driven vs externally driven
- Easy going work discipline vs strict work discipline
- Local vs professional
- Open system vs closed system
- Employee oriented vs work oriented
- Degree of acceptance of leadership style
- Degree of identification with your organization

This starts to become complicated!
