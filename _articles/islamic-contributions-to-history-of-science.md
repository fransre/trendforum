---
source: TrendForum
created: '2010-10-29'
author: Jürgen
title: Islamic contributions to history of science
categories:
- Geschiedenis
- Wetenschap
---
George Saliba: Islam & the transformation of Greek Science

[Islam & the transformation of Greek Science](https://www.youtube.com/watch?v=yf7sxcYzLEY)

[Islamic Science and the Making of the European Renaissance](https://www.amazon.com/Islamic-Science-European-Renaissance-Transformations/dp/0262195577)

[George Saliba](https://en.wikipedia.org/wiki/George_Saliba)

Thanks!! An excellent example of the direction of science being determined by society.
He mentions about Galileo "that that case is not transferable, is not universal", but what if a scientist discovers something contrary to beliefs of a religious ruling class? A pity he did not discuss that.

At the end Saliba mentions something very interesting: "The peculiar phenomenon that has to be explained is why did Europe take off in the 16th century, and all other civilisations began to look like they declined after the 16th century - not before. (...) That is the subject of my next book."

A new book [The House of Wisdom: How Arabic Science Saved Ancient Knowledge and Gave Us the Renaissance](https://www.amazon.com/House-Wisdom-Science-Knowledge-Renaissance/dp/1594202796/) from author Jim Al-Khalili](https://en.wikipedia.org/wiki/Jim_Al-Khalili).

From [a review](https://www.nytimes.com/2011/05/22/books/review/book-review-the-house-of-wisdom-by-jim-al-khalili.html):

"Some of the notable scientists [from the parallel culture that rose in the Middle East in the thousand years between the decline of Rome and the springtime of the Renaissance] were Christians, Jews and Persians, after all, and they had in common Arabic as the lingua franca. He also reminds readers that in early Islam there was no bitter conflict between religion and science and that the Koran encouraged the close study of all God’s works."
