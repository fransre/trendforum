---
source: TrendForum
created: '2011-01-30'
author: Jürgen
title: Rachel Botsman on Collaborative Consumption
categories:
- Globalisering
- Maatschappij
---
Rachel on [TED](https://www.ted.com/talks/rachel_botsman_the_case_for_collaborative_consumption.html)

Two videos ([one](https://www.publicchristianity.com/Videos/collaborative_consumption.html) and [two](https://www.publicchristianity.com/Videos/collaborative_consumption2.html)) with an interview with Rachel from the centre of public Christianity.

There are three different kind of markets:

+ redistribution markets (e.g. ebay)
+ product service systems (e.g. bike sharing)
+ collaborative lifestyles (e.g. farmers markets, p2p lending, couch surfing)

The technical basis are mobile phones and the Internet because they enable the finding of a good match.
[short CC video](https://vimeo.com/11924774)

Her [website](https://www.rachelbotsman.com/) and the site about [Collaborative Consumption](https://www.collaborativeconsumption.com/)
