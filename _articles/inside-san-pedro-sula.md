---
source: TrendForum
created: '2014-10-11'
author: Jürgen
title: 'Inside San Pedro Sula '
categories:
- Globalisering
- Maatschappij
---
Inside San Pedro Sula – the [most violent city](https://www.theguardian.com/world/2013/may/15/san-pedro-sula-honduras-most-violent) in the world
