---
source: TrendForum
created: '2016-05-06'
author: Jürgen
title: How Is Capitalism Going to End?
categories:
- Globalisering
- Techniek
- Maatschappij
- Financiën
---
An [Interview](https://www.vice.com/read/paul-mason-interview-postcapitalism-845) with Journalist Paul Mason.


[Postcapitalism: A Guide to Our Future](https://www.amazon.com/Postcapitalism-Guide-Future-Paul-Mason/dp/0374235546)
by Paul Mason
