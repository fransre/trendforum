---
source: TrendForum
created: '2011-01-15'
author: Jürgen
lang: nl
title: Otto Kroesen heeft een vernieuwde website
categories:
- Ontwikkelingssamenwerking
- Geschiedenis
- Kerk
- Globalisering
- Techniek
---
[Temporavitae](https://www.temporavitae.nl)
onder Literatuur vind je veel van zijn artikelen in digitale vorm

Het boek: [Leven in organisaties](https://www.temporavitae.nl/Leven%20in%20Organisaties.html) is vertaald naar het Engels: [Vacant Responsibilities: an ethics of timing](https://www.temporavitae.nl/Eng-Vacant%20Responsibilities.htm).
