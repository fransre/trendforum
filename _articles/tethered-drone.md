---
source: TechSquare
created: '2016-02-13'
author: Jürgen
title: Tethered drone
categories:
- Privacy
- Security
- Devices
---
How a [drone on a leash](https://www.techrepublic.com/article/how-a-drone-on-a-leash-will-transform-autonomous-flying/) will transform autonomous flying
