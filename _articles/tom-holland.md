---
source: TrendForum
created: '2009-07-13'
author: Jürgen
title: Tom Holland
categories:
- Geschiedenis
- Kerk
- Globalisering
- People
---
![Tom Holland](https://www.tom-holland.org/wp-content/uploads/2019/09/tom-holland-1.jpg)

[Tom Holland](https://www.tom-holland.org) [Wikipedia](https://en.wikipedia.org/wiki/Tom_Holland_(author)) is an author about vampires and history.

His first two historical novels are about Rome: [Rubicon: The Last Years of the Roman Republic](https://en.wikipedia.org/wiki/Rubicon:_The_Last_Years_of_the_Roman_Republic) [Amazon](https://www.amazon.com/Rubicon-Last-Years-Roman-Republic/dp/1400078970)
and Persia:
Persian Fire: The First World Empire and the Battle for the West [Amazon](https://www.amazon.com/Persian-Fire-First-Empire-Battle/dp/0307279480)

His latest book is about the change of the first millenium called
[Millennium: The End of the World and the Forging of Christendom](https://www.tom-holland.org/books/millennium.html) [Amazon](https://www.amazon.de/Millennium-End-World-Forging-Christendom/dp/1408700867) or
[The Forge of Christendom: The End of Days and the Epic Rise of the West](https://www.tom-holland.org/books/the-forge-of-christendom.html) [Amazon](https://www.amazon.com/Forge-Christendom-Days-Epic-Rise/dp/0385520581)

To draw attention to his book he wrote an article [Europe's First Revolution](https://www.newstatesman.com/ideas/2008/10/europe-christian-essay-less). Because of the many and angry reactions, as he notes in his [blog](https://www.tom-holland.org/blog/post/pagans-and-christians.html), he was invited to write a second one: [Uncomfortable origins](https://www.newstatesman.com/religion/2008/11/essay-christian-secularism).

Both articles are very much in the line of reasoning of Eugen Rosenstock-Huessy who also looks at origins of current cultural phenomena.

In zijn nieuwe boek Dominion: The Making of the Western Mind [Amazon](https://www.amazon.com/Dominion-Christian-Revolution-Remade-World/dp/0465093507) beschrijft Tom Holland hoe onze wereld christelijk geworden is en hoe de meesten van ons het ook geworden zijn onafhankelijk of we bewust Christen zijn, of Atheïsten of Boeddhisten of ons als neutrale wetenschappers zien.

Het boek is in 3 delen ingedeeld en bestaat uit 21 hoofdstukken. Ieder hoofdstuk heeft een speciaal historisch gebeuren als middelpunt.
Holland begint met Hellespont 479 vC en eindigt met Merkel in Rostock in 2015 AD.
Twee kernpunten zijn de pausrevolutie (investituurstrijd) en de sociologische Nietzsche-interpretatie. Hier is hij dicht bij Rosenstock-Huessy.

Het einde van de christelijke tijd ziet hij bij de Nationaal-socialisten waar de sterke goed is en gelijk heeft, net als bij de Romeinen, Grieken en Perzen.
Hij vertelt hoe hij bij het beschrijven van de antieke culturen ontdekte dat hij de ongebroken trots niet kon delen van Caesar die miljoen Galliërs doodde en een miljoen mensen tot slaaf maakte.
Hij zag daarin zijn eigen christelijkheid, dat hij namelijk vond dat het zwakke ook recht heeft en zelfs sterk kan zijn.
Hij citeert een niet-nader-genoemde Indiase filosoof die zei dat het christendom zich op twee manieren uitgebreid heeft, namelijk door bekering en door secularisatie.

Wat ik bij Rosenstock-Huessy altijd miste en wat Holland goed beschrijft, is hoe veranderingen vanuit hun eigen tijd te zien zijn.
Wat was de culturele/politieke situatie en welke verandering heeft daarin plaats gevonden.
Voor mij is Tom Holland een wegbereider voor Rosenstock-Huessy.
Als introductie adviseer ik het volgende gesprek: [Delingpod 35: Tom Holland](https://www.youtube.com/watch?v=R8J2aChrf7Q)

Hier een geweldig gesprek met Tom Holland in veel aspecten:

[Tom Holland on Christianity as Blasphemous Parody of the Worship of Caesar #Dominion](https://www.youtube.com/watch?v=_JADIcgUvw4)

And again a good podcast: [Episode 127: Tom Holland and Andrew Brown (edited)](https://www.churchtimes.co.uk/media/5662391/episode-127-tom-holland-and-andrew-brown-edited.mp3)

[Nice overview by Tom Holland](https://twitter.com/holland_tom/status/1181686010352156672)

[Tom Holland on the Great Awokening](https://www.youtube.com/watch?v=a27Z1qrguaw)

[Tom Holland & AC Grayling • History: Did Christianity give us our human values?](https://www.youtube.com/watch?v=7eSyz3BaVK8)

[Jonathan Pageau: Discussing Dominion with Tom Holland](https://www.youtube.com/watch?v=-qGfkFlZKgA)
- Christianity as harmonisation of opposites
- anti-Christian movements break this open
- de Sade as forerunner to Nietzsche
- Paul is not entirely clear what he wants to say
- Merkel is influenced by the parable of the good samaritan
- Anti-Christ, devils and angels as social spiritual powers
- Dostojewski’s (orthodox) view of latin Christendom which is so dynamic that it leads to atheism
- Movies taking over the story of the church
- and much more ...

Waarom de uitspraak: Maar democratie komt toch van de Grieken” diep christelijk is.

[Tom Holland | How Christianity Gained Dominion | A Secular Historian Loses His Faith (In Liberalism)](https://www.youtube.com/watch?v=favILmUsVdg)

[Tom Holland - Where do our ideas come from?](https://www.youtube.com/watch?v=Dq4IPV7gDAc)
- 45min: geschiedenis, waarom het kruis blasphemisch is voor Joden en Romeinen
- daarna: actuele vragen: Hitler, interpretatie van het knie, waar komen rechten vandaan?

[Why Science & Secularism Come From Christianity](https://www.youtube.com/watch?v=Lehk-ZSsbpI)

[Tom Holland: Christianity, persecution and the meaning of the cross](https://www.youtube.com/watch?v=p6w7qw9kJ9k)
Historian Tom Holland tells how his experiences among persecuted Christians and Yazidis in Iraq impacted his own intellectual and personal journey with Christianity. Titled 'Invisible Fire: Christianity in a post-western world', this 'Perspectives' lecture was recorded in Nov 2021 at the British Library for Christian persecution charity Open Doors with audience Q&A.

[Tom Holland On How Christianity Has Shaped Western Morality](https://www.youtube.com/watch?v=458Bz1GPto0)

Hier Tom krijgt 3 of 4 korte vragen en geeft heel lange antwoorden. Bijna mini-preken:
- wat is revolutionair aan het Christendom
- wat is de oorsprong van het humanisme
- science, slavery

[Christianity is the Greatest Story - Tom Holland and Paul Vanderklay](https://www.youtube.com/watch?v=_F_fzzkocmI)

deze is meer voor een christelijk publiek,  Tolkien etc.,
het gaat wel over moderne Europese talen en het anachronisme als we ze gebruiken om over de oudheid te spreken.

[On Christianity - An essay as a foreword for Tom Holland’s Dominion - by Nassim Nicholas Taleb](https://medium.com/incerto/on-christianity-b7fecde866ec)
