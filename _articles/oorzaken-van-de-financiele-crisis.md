---
source: TrendForum
created: '2013-04-08'
author: Jürgen
lang: nl
title: Oorzaken van de financiële crisis
categories:
- Globalisering
- Financiën
---
Ewald Engelen over [ongelijkheid, belastingparadijzen en schaduwbankieren](https://www.youtube.com/watch?v=tZpUYE6sicM).

one of the first to predict the crisis: [William White](https://en.wikipedia.org/wiki/William_White_%28economist%29)
[The Man Nobody Wanted to Hear: Global Banking Economist Warned of Coming Crisis](https://www.spiegel.de/international/business/the-man-nobody-wanted-to-hear-global-banking-economist-warned-of-coming-crisis-a-635051.html)

Willem Middelkoop [in gesprek](https://www.youtube.com/watch?v=wOerQA1TpV8) met financieel geograaf Ewald Engelen. Engelen heeft een zware tijd achter de rug. Kritiek op de lijst met namen in zijn boek en zijn reflectie daarop, bracht hem in een persoonlijke crisis. Hij heeft zich daar inmiddels uitgevochten en met Willem bespreekt hij zijn mentale houding tegenover de financiële wereld en de media.
