---
source: TrendForum
created: '2021-06-28'
author: Frans
title: 'Secrets about People: A Short and Dangerous Introduction to René Girard'
categories:
- Geschiedenis
- Kerk
- Globalisering
- Maatschappij
---
I understand [this introduction](https://alexdanco.com/2019/04/28/secrets-about-people-a-short-and-dangerous-introduction-to-rene-girard/) to René Girard until he drops the name "Trump". From there on it is my feeling that the author interprets Trumpism way too positive.
