---
source: TrendForum
created: '2015-12-12'
author: Jürgen
title: Achille Mbembe
categories:
- Globalisering
- Maatschappij
---
[Achille Mbembe](https://en.wikipedia.org/wiki/Achille_Mbembe) is a leading African postcolonial thinker.

[What is postcolonial thinking?](https://www.eurozine.com/articles/2008-01-09-mbembe-en.html)

[‘Europa is moe en zelfvoldaan’](https://www.nrc.nl/handelsblad/2015/12/11/europa-is-moe-en-zelfvoldaan-1565990)

His latest book: "Critique de la raison nègre"
Dutch: [Kritiek van de zwarte rede](https://www.bol.com/nl/p/kritiek-van-de-zwarte-rede/9200000045944823/)
German: [Kritik der schwarzen Vernunft](https://www.amazon.de/Kritik-schwarzen-Vernunft-Achille-Mbembe/dp/3518586149)

For the book he received the [Geschwister-Scholl-Preis](https://www.geschwister-scholl-preis.de/preistraeger_2010-2019/2015/index.php)

