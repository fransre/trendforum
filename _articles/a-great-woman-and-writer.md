---
source: TrendForum
created: '2010-11-10'
author: Jürgen
title: A great woman and writer
categories:
- Overig
---
Anne Lamott is a great person and writer. She writes about her life and experiences very open and honest.
[A Conversation with Anne Lamott 2007](https://www.youtube.com/watch?v=PhP5GmybvPM)

Other links:
* [Wikipedia](https://en.wikipedia.org/wiki/Anne_Lamott)
* [Anne Lamott](https://dir.salon.com/topics/anne_lamott/)
