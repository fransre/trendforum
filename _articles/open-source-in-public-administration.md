---
source: TechSquare
created: '2013-11-22'
author: Jürgen
title: Open source in public administration
categories:
- City
- Scalability
- Human Technical-System Interfacing
---
Munich just finished its [transition](https://www.techrepublic.com/article/how-munich-rejected-steve-ballmer-and-kicked-microsoft-out-of-the-city/) to open source.
