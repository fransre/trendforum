---
source: TrendForum
created: '2011-08-14'
author: Jürgen
title: Re-invent the toilet
categories:
- Ontwikkelingssamenwerking
---
To avoid diseases and not use water, the [Gates Foundation](https://www.gatesfoundation.org) started an [interesting initiative](https://www.youtube.com/watch?v=fdwvuTrycYU).
