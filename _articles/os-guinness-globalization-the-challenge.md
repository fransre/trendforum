---
source: TrendForum
created: '2010-10-22'
author: Jürgen
title: 'Os Guinness: Globalization - The Challenge'
categories:
- Kerk
- Globalisering
---
From the third Lausanne Conference: Cape Town 2010:
[Globalization: The Challenge](https://conversation.lausanne.org/en/conversations/detail/11428) by Os Guinness

3 important tasks:

* discern
* assess
* engage

Globalization:

* multiple modernities (American, European, Asian (Chinese, Japanese, Korean, Singaporean)
* countervailing descriptions of modernity
    * becoming global and becoming local (Europe/Basks)
* winners and losers of globalization

3 grand global tasks

* prepare the global south for globalization
    * the most of the south is premodern
    * the Church in the west is in profound cultural captivity
* come over to the west and help us to win the west back
    * a third mission to the west is necessary (1. Rome, 2. conversion of the Germanic tribes)
* help humanity forward at this global hour
    * the next generation is crucial for the global future

[Os Guinness: The Christian Church and the Western World](https://www.youtube.com/watch?v=A3-qimjJC3E)

[Os Guinness: Three Views of Evil](https://www.youtube.com/watch?v=n7v7fYNP3Qc)

[Os Guinness: A realistic view of evil](https://www.youtube.com/watch?v=Mvl961GCU8I)

["The Threat of Islam" by Sociologist Os Guinness](https://www.youtube.com/watch?v=nAg8suWRjvs)
