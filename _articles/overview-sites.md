---
source: Enkairoi
created: '2009-01-22'
author: Jürgen
title: Overview Sites
categories:
---

**Websites which present a good overview and introduction on important topics:**
- [12Manage.com](https://www.12manage.com/): E-learning community on management.
- [ChangingMinds.org](http://changingminds.org/): about how we change what others think, believe, feel and do.
