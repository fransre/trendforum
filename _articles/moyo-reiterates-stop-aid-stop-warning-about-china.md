---
source: TrendForum
created: '2011-09-19'
author: Jürgen
title: 'Moyo reiterates: Stop aid, stop warning about China'
categories:
- Ontwikkelingssamenwerking
- Globalisering
---
Dambisa Moyo reiterates her [statements](https://www.spiegel.de/international/world/0,1518,786465,00.html) about Development Aid.
