---
source: TechSquare
created: '2012-07-14'
author: Jürgen
title: GO becoming ready for production
categories:
- Cloud
- Scalability
- Architecture
- Devices
---
The programming language Go from Google is becoming ready production.

At Google I/O 2012 there is a [video](https://www.youtube.com/watch?v=kKQLhGZVN4A) about Companies switching for certain projects:
Canonical: from Python to Go
Heroku: from Ruby on Rails to Go
StatHat: Go
Iron.io: from Ruby on Rails (30 Ruby servers → 2 Go servers), NodeJS: wait for call-backs → just another goroutine
Smugmug: Go


The Go [website](https://golang.org) and a more broad [collection](https://go-lang.cat-v.org/) and [wikipedia]()
fast, statically compiled, concurrent and scales because of absence of inheritance.

A great intro: [Go in three easy pieces](https://channel9.msdn.com/Events/Lang-NEXT/Lang-NEXT-2012/Go-In-Three-Easy-Pieces) by Robert Griesemer.

Ivo Balbaert's kindle book: [The Way to Go](https://www.amazon.de/Way-Go-Introduction-Programming-ebook/dp/B0083RVAJW) for € 2,84.

