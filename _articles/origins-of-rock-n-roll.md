---
source: TrendForum
created: '2018-03-19'
author: Jürgen
title: Origins of Rock 'n' Roll
categories:
- Geschiedenis
---
[Sister Rosetta Tharpe](https://en.wikipedia.org/wiki/Sister_Rosetta_Tharpe): The Queer Black Woman Who [Invented Rock ’n’ Roll](https://www.theroot.com/sister-rosetta-tharpe-the-queer-black-woman-who-invent-1823198999)

- Example of her Guitar picking style: [Sister Rosetta Tharpe - Didn't it rain, children](https://www.youtube.com/watch?v=3NFywQdeKSo)
- Lyle Lovett Inducts Johnny Cash into the Rock and Roll Hall of Fame who [acknowledges Sister Rosetta Tharpe](https://www.youtube.com/watch?v=XUTFX47B23c)
- book: [Shout Sister Shout](https://www.shoutsistershout.net/shoutsummary.html), [A](https://www.amazon.com/Shout-Sister-Rock-Roll-Trailblazer/dp/0807009857),[W](https://en.wikipedia.org/wiki/Shout,_Sister,_Shout:_A_Tribute_to_Sister_Rosetta_Tharpe)
