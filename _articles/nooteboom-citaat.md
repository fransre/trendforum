---
source: TrendForum
created: '2010-12-13'
author: Jürgen
lang: nl
title: Nooteboom citaat
categories:
- Geschiedenis
---
Nooteboom deed een suggestie het heden eens anders te bekijken: “Als men het heden als geschiedenis beschouwt, kan men in een eigenaardige omkering ook het verleden als heden beschouwen.”
[Cees Nooteboom ontvangt Duitse literatuurprijs](https://www.nrc.nl/kunst/article2648682.ece/Cees_Nooteboom_ontvangt_Duitse_literatuurprijs)
Klinkt interessant, maar wat wordt daarmee bedoeld?

Daarvoor is het opinieartikel van Nooteboom zelf nuttig: [Europa en het werk der herinnering](https://digitaleeditie.nrc.nl/NH/2010/11/20101213___/1_08/article1.html). "Je leert, kortom, het heden zien als onvoltooide geschiedenis, herkent misschien zelfs patronen, dilemma’s, noodlottigheden, herhalingen, en vanaf dat ogenblik is geschiedenis met alles wat ze aan tastbare herinneringen achterlaat een constante in je leven geworden."
