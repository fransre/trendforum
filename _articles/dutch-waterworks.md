---
source: TechSquare
created: '2020-02-22'
author: Jürgen
title: Dutch Waterworks
categories:
- City
- Architecture
- Infrastructure
---
[How The Dutch Dug Up Their Country From The Sea](https://www.youtube.com/watch?v=8ir1Vj1D930)

[Why The Dutch Turned A Sea Into A Lake](https://www.youtube.com/watch?v=OdVEVP9mRRU)

[Why The Netherlands Isn't Flooding (Anymore)](https://www.youtube.com/watch?v=B-M2sORduKI)
