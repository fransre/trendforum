---
source: TechSquare
created: '2013-09-19'
author: Jürgen
title: Microchip at 55
categories:
- Baseline
---
[Happy 55th birthday to the Microchip!](https://blog.nxp.com/happy-55th-birthday-to-the-microchip/)
