---
source: TechSquare
created: '2011-01-08'
author: Jürgen
title: Privacy Approaches for Internet Video Advertising
categories:
- Privacy
---
Article on an interesting solution for [Privacy Approaches for Internet Video Advertising](https://www.intertrust.com/system/files/Privacy%20Approaches%20for%20Internet%20Video%20Advertising-1.pdf)

It makes the user's device the active part.
