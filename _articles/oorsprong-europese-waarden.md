---
source: TrendForum
created: '2023-05-25'
author: Jürgen
title: Over oorsprong Europese unie
categories:
- Maatschappij
---

[De verrassende oorsprong van onze Europese waarden met Clemens van den Berg en Beatrice de Graaf](https://www.nporadio1.nl/podcasts/de-ongelooflijke-podcast/70032/92-de-verrassende-oorsprong-van-onze-europese-waarden-met-clemens-van-den-berg-en-beatrice-de-graaf)

Waar komen de Europese waarden vandaan? Historicus en theoloog Clemens van den Berg heeft daar onderzoek naar gedaan en zag een kerkelijke beweging aan de oorsprong staan van deze Europese waarden en eigenlijk ook van 'het Europese project' zelf. De Europese geschiedenis is veel gelaagder en interessanter dan de saaie verdragen die vaak genoemd worden, en het is ook actueler.
