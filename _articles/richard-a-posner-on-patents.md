---
source: TechSquare
created: '2012-07-22'
author: Jürgen
title: " Richard A. Posner on patents"
categories:
- Policy
---
Posner after dismissing a high-profile suit between Apple and Motorola [states](https://www.theatlantic.com/business/archive/2012/07/why-there-are-too-many-patents-in-america/259725/): "...that there appear to be serious problems with our patent system."
He sees a number of possible solutions:

- "reducing the patent term for inventors in industries that do not have the peculiar characteristics of pharmaceuticals that I described;
- instituting a system of compulsory licensing of patented inventions;
- eliminating court trials including jury trials in patent cases by expanding the authority and procedures of the Patent and Trademark Office to make it the trier of patent cases, subject to limited appellate review in the courts;
- forbidding patent trolling by requiring the patentee to produce the patented invention within a specified period, or lose the patent; and
- (what is beginning) provide special training for federal judges who volunteer to preside over patent litigation."
