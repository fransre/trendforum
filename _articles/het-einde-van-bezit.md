---
source: TrendForum
created: '2015-11-21'
author: Jürgen
lang: nl
title: Het einde van bezit
categories:
- Globalisering
- Maatschappij
---
Wat betekent het voor het productieproces als we niet langer spullen bezitten, maar alleen gebruiken? [Een Tegenlicht Lab met Thomas Rau](https://tegenlicht.vpro.nl/afleveringen/2015-2016/einde-van-bezit.html)

[‘Duurzaamheid is FAKE’](https://www.energievastgoed.nl/2015/06/05/thomas-rau-duurzaamheid-is-fake/)
