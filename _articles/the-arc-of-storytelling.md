---
source: TrendForum
created: '2013-09-12'
author: Jürgen
title: The Arc of Storytelling
categories:
- Maatschappij
---
[The Arc of Storytelling](https://www.qideas.org/video/the-arc-of-storytelling.aspx) by Bobette Buster

In our culture, he who tells the best story wins. Creating great narratives that produce epiphanies involves a particular talent that applies far beyond film and changes the way you write books, marketing copy, funding proposals, research reports, sermons, and so much more. Bobette Buster has built and sustained a long, respected career in the film industry by being the best at finding and developing epiphanies in some of the greatest movies we've all enjoyed.

She mentions Bettelheim's [The Uses of Enchantment: The Meaning and Importance of Fairy Tales](https://www.amazon.com/The-Uses-Enchantment-Meaning-Importance/dp/0307739635) ([W](https://en.wikipedia.org/wiki/The_Uses_of_Enchantment)).
