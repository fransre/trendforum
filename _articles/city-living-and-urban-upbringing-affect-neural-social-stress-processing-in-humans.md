---
source: TrendForum
created: '2011-07-02'
author: Jürgen
title: City living and urban upbringing affect neural social stress processing in
  humans
categories:
- Globalisering
- Wetenschap
---
Cities have both health risks and benefits, but mental health is negatively affected: mood and anxiety disorders are more prevalent in city dwellers and the incidence of schizophrenia is strongly increased in [people born and raised in cities](https://www.nature.com/nature/journal/v474/n7352/full/nature10190.html#affil-auth).

Interesting article. All the more reason to spend enough time in the countryside!
