---
source: TrendForum
created: '2013-08-10'
author: Jürgen
title: Connectome
categories:
- Wetenschap
- Techniek
---
A [connectome](https://en.wikipedia.org/wiki/Connectome) is a comprehensive map of neural connections in the brain.
Sebastian Seung [talks](https://www.youtube.com/watch?v=HA7GwKXfJB0)about mapping a massively model of the brain that focuses on the connections between each neuron.
