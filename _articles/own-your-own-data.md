---
source: TechSquare
created: '2014-07-18'
author: Frans
title: Own your own data
categories:
- Cloud
- Privacy
---
[Own your own data](https://newsoffice.mit.edu/2014/own-your-own-data-0709) by MIT:

“You share code; you don’t share data. Instead of you sending data to Pandora, for Pandora to define what your musical preferences are, it’s Pandora sending a piece of code to you for you to define your musical preferences and send it back to them.”
