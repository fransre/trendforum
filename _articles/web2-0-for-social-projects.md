---
source: TrendForum
created: '2011-08-14'
author: Jürgen
title: Web2.0 for social projects
categories:
- Ontwikkelingssamenwerking
- Globalisering
---
Web technology is being used for social and development projects.

* [Betterplace](https://www.betterplace.org/en/how_it_works) coordinates projects and donors on the Internet.
* [Micro-volunteering](https://en.wikipedia.org/wiki/Micro-volunteering) is the practical support for social projects via Internet.
