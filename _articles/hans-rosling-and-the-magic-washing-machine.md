---
source: TrendForum
created: '2011-03-23'
author: Willem
title: Hans Rosling and the magic washing machine
categories:
- Globalisering
- Techniek
---
Not a new lesson, but well told: [The magic washing machine](https://www.ted.com/talks/view/id/1101)

I think he's right in mentioning that as the west we can't export (power) consumption globally like we use it ourselves.

Great stuff for thinking.

What will be the new ww infrastructure? I assume that everybody can have a TV set and a mobile phone. Can African countries live on solar and wind power? What about Asian countries? What is the lesson from Fukushima? High availability, reliability, safety and security of commercial, electricity, communication and water infrastructure.

Bill Gates about [free energy](https://www.youtube.com/watch?v=JaF-fq2Zn7I)
