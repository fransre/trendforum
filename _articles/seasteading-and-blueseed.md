---
source: TrendForum
created: '2014-04-14'
author: Jürgen
title: Seasteading and Blueseed
categories:
- Techniek
- Maatschappij
---
[Seasteading](https://en.wikipedia.org/wiki/Seasteading) is the concept of creating permanent dwellings at sea, called seasteads, outside the territory claimed by the government of any standing nation. Most proposed seasteads have been modified cruising vessels. Other proposed structures have included a refitted oil platform, a decommissioned anti-aircraft platform, and custom-built floating islands.[1] No one has created a state on the high seas that has been recognized as a sovereign nation, although the Principality of Sealand is a disputed [micronation](https://en.wikipedia.org/wiki/Micronation) formed on a discarded sea fort near Suffolk, England.
[[video](https://www.youtube.com/watch?v=0Xu6ROUHOz8)]

The closest things to a seastead that have been built so far are large ocean-going ships sometimes called "[floating cities](https://en.wikipedia.org/wiki/Walking_city#Floating_cities)", and smaller [floating islands](https://en.wikipedia.org/wiki/Floating_island).

[Blueseed](https://blueseed.co/) is creating the world’s most amazing upcoming startup community for entrepreneurs, on a cruise ship 12 nautical miles from the coast of San Francisco, in international waters. [[video](https://www.youtube.com/watch?v=iinQKhaSrI4)]
