---
source: TechSquare
created: '2014-09-15'
author: Jürgen
title: Certificate Transparency
categories:
- Privacy
- Security
---
Google's Certificate Transparency [project](https://www.certificate-transparency.org/) fixes several structural flaws in the SSL certificate system, which is the main cryptographic system that underlies all HTTPS connections.

Certificate Transparency helps eliminate these flaws by providing an open framework for monitoring and auditing SSL certificates in nearly real time. Specifically, Certificate Transparency makes it possible to detect SSL certificates that have been mistakenly issued by a certificate authority or maliciously acquired from an otherwise unimpeachable certificate authority. It also makes it possible to identify certificate authorities that have gone rogue and are maliciously issuing certificates.
