---
source: TrendForum
created: '2010-12-21'
author: Jürgen
title: 'About Economics: The Return of History by David Brooks'
categories:
- Geschiedenis
- Wetenschap
---
[The Return of History](https://www.nytimes.com/2010/03/26/opinion/26brooks.html)

"Economics achieved coherence as a science by amputating most of human nature. Now economists are starting with those parts of emotional life that they can count and model (the activities that make them economists). But once they’re in this terrain, they’ll surely find that the processes that make up the inner life are not amenable to the methodologies of social science. The moral and social yearnings of fully realized human beings are not reducible to universal laws and cannot be studied like physics.

Once this is accepted, economics would again become a subsection of history and moral philosophy. It will be a powerful language for analyzing certain sorts of activity. Economists will be able to describe how some people acted in some specific contexts. They will be able to draw out some suggestive lessons to keep in mind while thinking about other people and other contexts — just as historians, psychologists and novelists do.

At the end of Act V, economics will be realistic, but it will be an art, not a science. "

Wat maakt iets wetenschappelijk benaderbaar of niet? -- "In real sciences, evidence solves problems. Roberts asked his colleagues if they could think of any econometric study so well done that it had definitively settled a dispute. Nobody could think of one."
