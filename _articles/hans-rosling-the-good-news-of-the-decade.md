---
source: TrendForum
created: '2010-10-12'
author: Frans
title: 'Hans Rosling: The good news of the decade?'
categories:
- Ontwikkelingssamenwerking
---
Het blijkt beter te gaan met het bestrijden van kindersterfte (1 van de milleniumdoelen) dan je zou denken op basis van rapporten van de Verenigde Naties.

Geweldige presentatie van Hans Rosling! Deze soort differentiatie is belangrijk.
De oude manier van spreken over de derde wereld werkt echter niet meer. Dat vergt adaptatie en vereist een diepere kennis.
Kijk daarvoor: [Hans Rosling on global population growth](https://www.ted.com/talks/hans_rosling_on_global_population_growth.html).

Another comment from David Brooks on America being the country of middle-class:
[Ben Franklin's nation](https://www.nytimes.com/2010/12/14/opinion/14brooks.html)
