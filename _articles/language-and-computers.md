---
source: TechSquare
created: '2014-06-09'
author: Jürgen
title: Language and computers
categories:
- Education
- Human Technical-System Interfacing
---
Some essays about computers and languages:

[Why language isn't computer code](https://www.economist.com/blogs/johnson/2012/07/language-and-computers)
[Automated grammar-checking](https://www.economist.com/blogs/johnson/2012/08/computers-and-language-continued)
[Parsing in pajamas](https://www.economist.com/blogs/johnson/2012/08/computers-and-language-continued-0)
