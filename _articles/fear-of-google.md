---
source: TrendForum
created: '2014-04-18'
author: Jürgen
title: Fear of Google
categories:
- Globalisering
- Techniek
- Maatschappij
---
The FAZ started a discussion about the markets-dominating power of Google:

- Robert M. Maier, Founder and CEO of Visual Media GmbH: [Von der Suchmaschine zur Weltmacht - Angst vor Google ](https://www.faz.net/aktuell/feuilleton/debatten/weltmacht-google-ist-gefahr-fuer-die-gesellschaft-12877120.html?printPagedArticle=true#pageIndex_2)
- Eric Schmidt: [about the good things Google does - A chance for growth](https://www.faz.net/aktuell/feuilleton/debatten/eric-schmidt-about-the-good-things-google-does-a-chance-for-growth-12887909.html)
- Mathias Döpfner, CEO of Axel Springer GmbH: [An open letter to Eric Schmidt - Why we fear Google](https://www.faz.net/aktuell/feuilleton/debatten/mathias-doepfner-s-open-letter-to-eric-schmidt-12900860.html?printPagedArticle=true#pageIndex_2)
- NYT: [In Germany, Strong Words Over Google's Power](https://www.nytimes.com/2014/04/17/business/international/in-germany-strong-words-over-googles-power.html?_r=0)
- About reactions of other parties: [Ein Goliath macht sich ganz klein ](https://www.faz.net/aktuell/feuilleton/debatten/reaktionen-auf-doepfners-google-kritik-12900528.html)
