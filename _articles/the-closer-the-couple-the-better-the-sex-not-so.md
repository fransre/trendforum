---
source: TrendForum
created: '2015-09-23'
author: Jürgen
title: The closer the couple, the better the sex? Not so
categories:
- Maatschappij
---
[Esther Perel](https://en.wikipedia.org/wiki/Esther_Perel) explains
[The closer the couple, the better the sex? Not so](https://www.telegraph.co.uk/women/sex/10857870/The-closer-the-couple-the-better-the-sex-Not-so.html)
because in short, love and security need closeness; passion and desire need space.

Or [Mating in Captivity: Reconciling Intimacy and Sexuality](https://www.youtube.com/watch?v=K3vY5Q-NoMY)

Sue Johnson: The [New Frontier](https://www.youtube.com/watch?v=hiVijMLH2-k) of Sex & Intimacy
