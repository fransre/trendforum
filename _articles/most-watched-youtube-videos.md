---
source: TrendForum
created: '2012-10-18'
author: Jürgen
title: Most watched youtube videos
categories:
- Globalisering
- Maatschappij
---
['Gangnam Style' overtakes 'Call Me Maybe'](https://www.gmanetwork.com/news/story/275538/scitech/socialmedia/gangnam-style-overtakes-call-me-maybe-as-most-watched-youtube-video)
From 300,248,629 (since Mar 1, 2012) to 481,619,916 (since Jul 15, 2012) views.
