---
source: TechSquare
created: '2011-01-05'
author: Jürgen
title: 'Interesting p2p initiative '
categories:
- Architecture
---
[Dot-p2p Goals](https://dot-p2p.org/index.php?title=Goals)
Create an application that runs as a service and hooks into the host's DNS system to catch all requests to the .p2p TLD while passing all other requests cleanly through. Requests for the .p2p TLD will be redirected to a locally hosted DNS database.

Leuk idee!
