---
source: EensVoorAl
created: '2013-01-19'
author: Jürgen
lang: nl
title: Overzicht ERH concepten
---
Basis termen:
- geest
- ziel
- tijd, toekomst, verleden
- ruimte

Concepten van ERH:
- 4 oudheden (en de moderne opvolgers)
- Jezus als middenpunt van de wereldgeschiedenis
- 3 millennia
- unieke van het christendom: overwinning/vruchtbaar maken van de dood
- christelijke toekomst
- christenzijn als proces
- de geest zoekt naar nieuwe vormen, beddingen
- kruis der werkelijkheid
- 12 tonen van de geest
- wet van de techniek: iedere technische vooruitgang vergroot de ruimte, verkort de tijd en doet een sociale groep teniet.
- eerste eco-dynamisch wet: in de industrie zijn drie mensen gelijk aan een mens, probleem van de productie: 3 = 1
- tweede eco-dynamisch wet: macht relatie tussen een coöperatie en een enkeling vraagt collectieve antwoord: allen = 1
- derde eco-dynamisch wet: diepe sociale relaties leven van de dualiteit: 2 = 1
- vierde eco-dynamisch wet: probleem van de creativiteit, het woorden van een ziel: 1 = 1
- 4 sociale ziekten: revolutie, oorlog, anarchie en decadentie
- 4 overwinnaars van de dood: Abraham, Boeddha, Laozi, Jezus
- 4 disangelisten: Darwin, Marx, Nietzsche and Freud
- grammaticale methode voor de sociale wetenschappen: psychologie, sociologie, economie, geschiedenis
- alexandrinische grammatica
- jij, ik, wij, het
- imperatieven,
- argonautiek (scholastiek, akademiek)
- metanomiek
- vrucht der lippen
- 4 evangelies als compositie
- eerste voorbeeld van moderne wetenschap: stambomen van Mattheus en Lukas bijven naast elkaar staan
- het kruis der werkelijkheid van een bedrijf: arbeider, ingenieur, management, kooplui
- ......
