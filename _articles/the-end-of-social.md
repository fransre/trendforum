---
source: TechSquare
created: '2011-12-15'
author: Frans
title: The end of social
categories:
- Privacy
---
When you take the friction out of sharing, you also remove the value.

On [O'Reilly Radar](https://radar.oreilly.com/2011/12/the-end-of-social.html).

To me it looks like that the article describes something of a "basic social architecture" perhaps similar to our categories Baseline/"Architecture".
I only find it hard to name it explicitly.
