---
source: TrendForum
created: '2015-08-10'
author: Jürgen
title: Martine Vonk
categories:
- Geschiedenis
- Techniek
- Maatschappij
- People
---
[Martine Vonk](https://www.martinevonk.nl/over-mij/) is a lecturer on the impact of technology on society. Her [dissertation](https://www.martinevonk.nl/boeken-artikelen/) is on "Sustainability and Quality of Life. A study on the religious worldviews, values and environmental
impact of Amish, Hutterite, Franciscan and Benedictine communities"
