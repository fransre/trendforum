---
source: TechSquare
created: '2014-01-15'
author: Jürgen
title: The privacy smartphone
categories:
- Privacy
- Devices
---
[Blackphone](https://www.blackphone.ch/) is the world's first smartphone which prioritizes the user's privacy and control, without any hooks to carriers or vendors.

produced by [Geeksphone](https://en.wikipedia.org/wiki/GeeksPhone)

Mozilla [Will Stop](https://techcrunch.com/2015/12/08/mozilla-will-stop-developing-and-selling-firefox-os-smartphones/) Developing And Selling Firefox OS Smartphones
