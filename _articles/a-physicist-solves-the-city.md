---
source: TechSquare
created: '2010-12-28'
author: Frans
title: A Physicist Solves the City
categories:
- City
---
Modern cities are the real centers of sustainability. According to the data, people who live in densely populated places require less heat in the winter and need fewer miles of asphalt per capita. (...) Small communities might look green, but they consume a disproportionate amount of everything. As a result, creating a more sustainable society will require our big cities to get even bigger.

[A Physicist Solves the City](https://www.nytimes.com/2010/12/19/magazine/19UrbanWest-t.html)

wow, great article!
