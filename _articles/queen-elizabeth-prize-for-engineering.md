---
source: TrendForum
created: '2015-05-05'
author: Jürgen
title: Queen Elizabeth Prize for Engineering
categories:
- Globalisering
- Techniek
- Maatschappij
---
[The Queen Elizabeth Prize for Engineering](https://qeprize.org/), is a [global engineering prize](https://en.wikipedia.org/wiki/Queen_Elizabeth_Prize_for_Engineering) that rewards and celebrates the engineers responsible for a ground-breaking innovation in engineering that has been of global benefit to humanity.


[QEPrize Winners 2013](https://qeprize.org/winners-2013/): The Internet and the Web
The Internet and the Web have revolutionised the way we communicate and enabled the creation of whole new industries.

- Robert Kahn, Vinton Cerf and Louis Pouzin made seminal contributions to the protocols that together make up the fundamental architecture of the Internet.
- Tim Berners-Lee created the World Wide Web and vastly extended the use of the Internet beyond email and file transfer.
- Marc Andreessen, while a student and working with colleagues, wrote the mosaic browser, which made the Web accessible to anyone.
