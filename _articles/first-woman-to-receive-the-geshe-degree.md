---
source: TrendForum
created: '2012-12-23'
author: Jürgen
title: First woman to receive the geshe degree
categories:
- Kerk
- Maatschappij
---
In [Breaking Through](https://www.thebuddhadharma.com/web-archive/2012/2/10/breaking-through.html) Amy Yee tells the story of the first woman to receive the Buddhist geshe degree.

The Buddha’s Forgotten Nuns:

Filmmaker Wiriya Sati has announced the [online release](https://vimeo.com/33052419) of The Buddha’s Forgotten Nuns. Ten years in the making, the documentary explores the attempts to revive the order of bhikkhunis, or fully ordained nuns, created by Shakyamuni Buddha more than 2,500 years ago. Asking two questions — “Is Buddhism a religious movement based on equality? Or is it rooted in a male-dominated culture found in most other world religions?” — Sati travels throughout Asia and the West, discovering both intractable conservatism and those intent on pushing beyond cultural barriers.
