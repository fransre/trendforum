---
source: TrendForum
created: '2018-03-18'
author: Jürgen
title: Cambridge Analytica processes 50M Facebook profiles to change US elections
categories:
- Wetenschap
- Techniek
- Maatschappij
---
Cambridge Analytica [whistleblower](https://www.youtube.com/watch?v=FXdYSQ6nu-M): 'We spent $1m harvesting millions of Facebook profiles'
Revealed: 50 million Facebook [profiles harvested](https://www.theguardian.com/news/2018/mar/17/cambridge-analytica-facebook-influence-us-election) for Cambridge Analytica in major data breach
The [Cambridge Analytica Files](https://www.theguardian.com/news/2018/mar/17/data-war-whistleblower-christopher-wylie-faceook-nix-bannon-trump):
‘I made Steve Bannon’s psychological warfare tool’: meet the data war whistleblower

Brill and Crovitz Announce [Launch of NewsGuard to Fight Fake News](https://www.publicisgroupe.com/en/news/press-releases/brill-and-crovitz-announce-launch-of-newsguard-to-fight-fake-news-en-1)
