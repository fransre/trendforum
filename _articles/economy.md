---
source: TrendForum
created: '2014-10-26'
author: Jürgen
title: Economy
categories:
- Globalisering
- Maatschappij
- Financiën
---
- [markets are efficient](https://en.wikipedia.org/wiki/Efficient-market_hypothesis)
- [humans act rational](https://en.wikipedia.org/wiki/Rational_choice_theory)
- [in the long run markets are in balance](https://en.wikipedia.org/wiki/Economic_equilibrium)

[Mathiness](https://en.wikipedia.org/wiki/Mathiness)
