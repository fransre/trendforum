---
source: TrendForum
created: '2011-04-12'
author: Jürgen
title: Finance-Watch
categories:
- Globalisering
- Maatschappij
---
Members of the European Parliament found a new [Lobby Organization](https://www.finance-watch.org/) to support them in the fight for the control of financial markets. Dutch members are almost absent.
