---
source: TechSquare
created: '2011-04-09'
author: Frans
title: Open Science
categories:
- Education
- Scalability
---
Michael Nielsen left academia to write a book about open science, and the radical change that online tools are causing in the way scientific discoveries are made.

[Open science: Michael Nielsen at TEDxWaterloo](https://www.youtube.com/watch?v=DnWocYKqvhw)

Interesting idea. However does not describe a solution how a scientist does get credit for his work when he works completely in the open.

Working in groups is a good idea. ERH's reference to the Argo where specialists from different fields team up for new discovery is a very good example.

Great talk; the polymath project is an amazing example as brought here. In some way open science is application of 'Western' values (in [Mahbubani's](https://mahbubani.net/book3.html) sense) to science.
If things happen online it would be possible to use technology (like crypto signatures: author for authorship, trusted party for timestamp) to verify that people made certain statements. With some moderation (that is needed anyway) this could lead to an automated aid in determining who contributed how much and what.
