---
source: TrendForum
created: '2012-05-18'
author: Jürgen
title: 'Hans Joas: New Geneology of Human Rights'
categories:
- Geschiedenis
- Globalisering
- Maatschappij
---
Hans Joas presents his new German book: [Die Sakralität der Person; Eine neue Genealogie der Menschenrechte](https://www.amazon.de/Die-Sakralit%C3%A4t-Person-Genealogie-Menschenrechte/dp/3518585665) at the Berkeley Center in 3 over an hour long lectures:
- [Punishment, Rights, and the Sacredness of the Person](https://vimeo.com/15235804)
- [Violence and the Origins of Human Rights](https://vimeo.com/15168606)
- [Human Rights and Universal Values](https://vimeo.com/15235858)

Against a pure philosophical argument or a historical telling of its genesis, he uses a method of historical sociology. Very good stuff.
