---
source: TechSquare
created: '2014-05-31'
author: Jürgen
title: Food and Apps
categories:
- Health
- Devices
---
15 [apps](https://www.techrepublic.com/pictures/photos-15-apps-that-are-changing-the-food-industry) that are changing the food industry
