window.addEventListener("load",
  function() {
    let newElement = function(tagName, attributes, nodes) {
      const element = document.createElement(tagName);
      Object.keys(attributes).forEach(attribute => element.setAttribute(attribute, attributes[attribute]));
      nodes.forEach(node => element.appendChild(node));
      return element;
    };

    let mapFrom = function(properties, callback) {
      return properties.reduce((map, property) => map.set(property, callback.call(this, property)), new Map());
    };

    const articleBlock = document.querySelector("#articles");
    const articles = Array.from(articleBlock.children);
    const properties = Object.keys(articles[0].dataset);
    const values = mapFrom(properties, property => articles.flatMap(article => article.dataset[property].split("|").filter(value => value)));
    const uniques = mapFrom(properties, property => Array.from(new Set(values.get(property))).sort());
    const urlParams = new URLSearchParams(window.location.search);
    const params = mapFrom(properties, property => urlParams.get(property));

    const filters = properties.map(property =>
      newElement("select", { id: property, name: property },
        [newElement("option", { value: "" }, [document.createTextNode(`All ${property}`)])].concat(
          uniques.get(property).map(unique => newElement("option", unique == params.get(property) ? { selected: "" } : {}, [document.createTextNode(unique)]))
        )
      )
    );
    const filterBlock = newElement("div", { id: "filters" }, [newElement("div", {}, [document.createTextNode("Filters")])].concat(filters));
    articleBlock.parentNode.insertBefore(filterBlock, articleBlock);

    filterBlock.addEventListener("change", event => {
      const selected = mapFrom(properties, property => document.querySelector(`#${property}`).value);

      const query = properties.filter(property => selected.get(property)).map(property => `${encodeURIComponent(property)}=${encodeURIComponent(selected.get(property))}`).join("&");
      history.replaceState(null, "", query ? `/?${query}` : "/");

      articles.forEach(article => {
        const matches = mapFrom(properties, property => selected.get(property) == "" || article.dataset[property].split("|").includes(selected.get(property)));
        article.style.display = Array.from(matches.values()).every(match => match) ? "block" : "none";
      });
    });

    filterBlock.dispatchEvent(new Event("change"));
  }
);
